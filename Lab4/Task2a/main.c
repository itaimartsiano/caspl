
#include "util.h"

#define SYS_WRITE 4
#define SYS_READ 3
#define OPEN_FILE 5
#define CLOSE_FILE 6
#define SEEK_IN_FILE 19
#define SYS_EXIT 1
#define GET_DENT 141
#define STDOUT 1
#define STDIN 0
#define O_RDONLY 0
#define BUFFER_SIZE 8192
extern int system_call();


void printError(){
	system_call(SYS_WRITE,STDOUT, "there was an error, exiting\n",28);
	system_call(1,0,0,0);
  	return;
}


typedef struct ent{
	int inode;
	int offset;
	short len;
	char name[];
} ent;


int main (int argc , char* argv[], char* envp[])
{
	system_call(SYS_WRITE, STDOUT, "Flame is working!\n", 18);

	char buf [BUFFER_SIZE];
	int fd;
	ent * entP = (ent*) buf;
	int count;
	int i = 0;

	fd = system_call(OPEN_FILE, ".", O_RDONLY, 0644);

	if (fd<0){
		printError();
		return 0;
	}

	count = system_call(GET_DENT, fd, buf, BUFFER_SIZE);

	if (count<0){
		printError();
		return 0;
	}

	while (i<count) {
		
		entP = (ent*) (buf + i);
		system_call(SYS_WRITE, STDOUT, entP->name, strlen(entP->name));
		system_call(SYS_WRITE, STDOUT, "\n", 1);
		i += (entP->len);
		
	}

	system_call(CLOSE_FILE, fd, 0, 0);


	return 0;

}
