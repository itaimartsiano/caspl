#include "util.h"

#define SYS_WRITE 4
#define STDOUT 1
#define OPEN_FILE 5
#define CLOSE_FILE 6
#define SEEK_IN_FILE 19
#define SYS_EXIT 1

extern 

int main (int argc , char* argv[], char* envp[])
{
  
  int file_desc = 0;
  file_desc = system_call(OPEN_FILE,argv[1], 2, 0777);

  if (file_desc == -1) {
  	system_call(SYS_EXIT,0, 0, 0);
  	return 0;
  }

  char * position = 0;
  position = (char *) system_call(SEEK_IN_FILE,file_desc, 657, 0);
  
  if (position == 0) {
	system_call(SYS_EXIT,0, 0, 0);
	return 0;
  }

  

  int num_Of_Bytes = system_call(SYS_WRITE,file_desc, argv[2], 5);

  if (num_Of_Bytes <= 0) {
  	system_call(SYS_EXIT,0, 0, 0);
  	return 0;
  }

  file_desc = system_call(CLOSE_FILE,file_desc, 0, 0);

  if (file_desc == -1) {
   	system_call(SYS_EXIT,0, 0, 0);
   	return 0;
  }

  system_call(SYS_WRITE,STDOUT, "End Succesfully!\n",17);

  return 0;
}
