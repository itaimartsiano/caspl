
#include "util.h"

#define SYS_WRITE 4
#define STDIN 0
#define STDOUT 1
#define SYS_READ 3
#define OPEN_FILE 5
#define CLOSE_FILE 6
#define SEEK_IN_FILE 19
#define SYS_EXIT 1
extern int system_call();


void getError(){
	system_call(SYS_WRITE,1, "there was an error, exiting\n",28);
	system_call(1,0,0,0);
  	return;
}


int main (int argc , char* argv[], char* envp[])
{
  int i=0,j=0,k=0;
  int isI = 0;
  int isO = 0;
  int lengthOfName = 0;
  int inputFile  = STDIN;
  int outputFile = STDOUT;
  int numberOfFiles=0;  
  int output[5] = {0};
  char fileName [11];
  int input = 0;
    
  for(i=1; i<argc; i++){						/*reading and initializing the requests*/
    
    if(strcmp(argv[i],"-i")==0)					/*get -i*/
      {
  		isI = 1;
  		i++;
  		input = system_call(OPEN_FILE, argv[i], 0, 0644);
    		
    		if (input < 0){
    			getError();
    			return 0;
    		}
    }

    else if(strcmp(argv[i],"-o")==0)			/*get -o*/
    {
  		numberOfFiles = positive_atoi(argv[i+1]);

  		if (numberOfFiles>5){
  			getError();
  			return 0;
  		}

  		system_call(SYS_WRITE,STDOUT, "Number of output files: ",24);
      system_call(SYS_WRITE,STDOUT, argv[i+1] , 1); 
  		i++;

  		for (j=0; j<(numberOfFiles);j++){     /*reading the file name*/
  	 	 	 isO = 1;
  		 	 system_call(SYS_WRITE,STDOUT, "\nEnter output file ",19);
  		 	 system_call(SYS_WRITE,STDOUT, itoa(j+1),1);
  		 	 system_call(SYS_WRITE,STDOUT, ":\n",2);
  		 	 
  		 	 lengthOfName = system_call(SYS_READ, STDIN ,fileName ,10);
  		 	 fileName [lengthOfName-1] = 0;
  		 	 output [j] = system_call(OPEN_FILE, fileName, 1|64, 0644);

	    	 if (output[j] < 0){
	  			getError();
	  		  return 0;
	  	   }
		  }
    }
  }


  if (isI) inputFile = input;

  if (!isI)	system_call(SYS_WRITE,STDOUT, "Enter your text:\n",18);
  
  if (isO)	outputFile = output[k];

  char buffer = '1';
  
  i = 0;

  int sys_Value = 1;
  do { 	/*print to files or console*/
    
    sys_Value = system_call(SYS_READ, inputFile ,&buffer ,1);

    if(!i){
      i++;
      system_call(SYS_WRITE,outputFile, "\n" , 1);    
      system_call(SYS_WRITE,outputFile, itoa(i) , 1);    
      system_call(SYS_WRITE,outputFile, ":" ,1);
    }

    system_call(SYS_WRITE,outputFile, &buffer, 1);

    if (strncmp(&buffer, "*",1) == 0 || strncmp(&buffer, "@",1) == 0){
      i++;
      
      if (isO){
      	k++;
		    outputFile = output[k%numberOfFiles];
  	  }

      system_call(SYS_WRITE,outputFile, "\n",1);
      system_call(SYS_WRITE,outputFile, itoa(i) , (i/10)+1);    
      system_call(SYS_WRITE,outputFile, ":" ,1);

    }
  }while (sys_Value>0);


 for (i=0 ; i < numberOfFiles ; i++){						/*close the files*/
 	system_call(SYS_WRITE,output[i], "\n",1);
 	system_call(CLOSE_FILE,output[i],0 ,0);	
 }

 return 0;

}
