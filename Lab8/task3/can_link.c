#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <elf.h>
#include <fcntl.h>
#include <sys/mman.h>


Elf32_Ehdr * map_start;
Elf32_Shdr *  sectionHeader;
Elf32_Shdr * secHeadStrTable;
char * symbols [10000];
int numOfSymb = 0;

void release_memory(){
	int i;
	for (i=0; i<numOfSymb; i++){
		free(symbols[i]);
	}
}


void mmap_file(char * file){

	int fileDesc;
	struct stat fs;
	void * mmap_start;

	if((fileDesc = open(file, O_RDWR)) < 0){
		perror("open file");
		release_memory();
		exit(0);
	}

	if (fstat(fileDesc, &fs) != 0){
		perror("fstat");
		release_memory();
		exit(0);
	}

	if ((mmap_start=mmap(0, fs.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fileDesc, 0))<0){
		perror("mmap");
		release_memory();
		exit(0);
	}

	map_start = (Elf32_Ehdr *) mmap_start;
	return;
}


int _start_func(){

	sectionHeader = (Elf32_Shdr *) (((void *)map_start)+(map_start->e_shoff)); 
	secHeadStrTable = sectionHeader + (map_start->e_shstrndx);	

	const char * const strtab = ((void *)map_start) + ((secHeadStrTable+2)->sh_offset);

	int offset = ((secHeadStrTable+1)->sh_offset);
	Elf32_Sym * symTable = (Elf32_Sym *) (((void *)map_start) + offset);

	int numOfSecHeadears = map_start->e_shnum;
	int sizeOfSymTable = sectionHeader [numOfSecHeadears-2].sh_size;
	int numOfSymbols = sizeOfSymTable/sizeof(Elf32_Sym);
	int i =0;
	for(i=0; i<numOfSymbols; i++)
	{
		int secIndex = symTable[i].st_shndx;

		if (secIndex>0 && secIndex<numOfSecHeadears){
			if (strcmp("_start", (strtab+(symTable[i].st_name))) == 0){
				return 1;
			}	
		}	
	}

	return 0;

}

int duplicateSymbols(){

	int i,j;
	for (i = 0; i<=numOfSymb; i++){
		for (j = i+1; j < numOfSymb; j++)
		{
			if(strcmp(symbols[i], symbols[j])==0){
				printf("duplicate check: FAILED (%s)\n", symbols[i]);
				release_memory();
				exit(-1);
				return (0);								/*return 0 if there is dup*/
			}
		}
	}
	return 1;

}

int insertSymbIntoArry(){

	sectionHeader = (Elf32_Shdr *) (((void *)map_start)+(map_start->e_shoff)); 
	secHeadStrTable = sectionHeader + (map_start->e_shstrndx);	

	const char * const strtab = ((void *)map_start) + ((secHeadStrTable+2)->sh_offset);

	int offset = ((secHeadStrTable+1)->sh_offset);
	Elf32_Sym * symTable = (Elf32_Sym *) (((void *)map_start) + offset);

	int numOfSecHeadears = map_start->e_shnum;
	int sizeOfSymTable = sectionHeader [numOfSecHeadears-2].sh_size;
	int numOfSymbols = sizeOfSymTable/sizeof(Elf32_Sym);
	int i =0;
	for(i=0; i<numOfSymbols; i++)
	{
		int secIndex = symTable[i].st_shndx;

		if (secIndex>0 && secIndex<numOfSecHeadears){
			const char * symbToInsert = (strtab+(symTable[i].st_name));
			if (strlen(symbToInsert) != 0){
				char * memoryPtr = malloc(strlen(symbToInsert));
				strcpy(memoryPtr, symbToInsert);
				symbols[numOfSymb] = memoryPtr;
				numOfSymb ++;
			}
		}	
	}
	return 0;
}




int undefienedSymbols(){

	sectionHeader = (Elf32_Shdr *) (((void *)map_start)+(map_start->e_shoff)); 
	secHeadStrTable = sectionHeader + (map_start->e_shstrndx);	

	const char * const strtab = ((void *)map_start) + ((secHeadStrTable+2)->sh_offset);

	int offset = ((secHeadStrTable+1)->sh_offset);
	Elf32_Sym * symTable = (Elf32_Sym *) (((void *)map_start) + offset);

	int numOfSecHeadears = map_start->e_shnum;
	int sizeOfSymTable = sectionHeader [numOfSecHeadears-2].sh_size;
	int numOfSymbols = sizeOfSymTable/sizeof(Elf32_Sym);
	int i =0;
	for(i=0; i<numOfSymbols; i++)
	{
		int undefined = (symTable[i].st_info) & 15;
		if ((undefined == 0) && i!=0) {
			int j;
			for (j = 0; j < numOfSymb; j++)
			{
				if(strcmp((strtab+(symTable[i].st_name)), symbols[j])==0){
					break;						
				}
			}
			if (j==numOfSymb){
				printf("no missing symbols: FAILED (%s)\n", (strtab+(symTable[i].st_name)));
				release_memory();
				exit(-1);
			}

		}	
	}
	return 1;

}




int main(int argc, char ** argv){

	if (argc < 3){
		printf("there is not enougth parameters");
		exit(-1);
	}

	/*
	mmap_file(argv[1]);
	if (_start_func()){
		printf("_start check: PASSED\n");
		return 1;		
	}else 
		{
		mmap_file(argv[2]);
		if (_start_func()){
			printf("_start check: PASSED\n");
			return 1;
		}
		}
	printf("_start check: FAILED\n");
	exit(-1);
	*/
	
	/*
	mmap_file(argv[1]);
	insertSymbIntoArry();
	mmap_file(argv[2]);
	insertSymbIntoArry();
	if (duplicateSymbols()){
		printf("duplicate check: PASSED\n");		
	}
	*/
	/*
	mmap_file(argv[1]);
	insertSymbIntoArry();
	mmap_file(argv[2]);
	insertSymbIntoArry();
	undefienedSymbols();
	mmap_file(argv[1]);
	undefienedSymbols();
	printf("no missing symbols: PASSED\n");
	*/

	mmap_file(argv[1]);
	insertSymbIntoArry();
	if (_start_func()){
		mmap_file(argv[2]);
		insertSymbIntoArry();
		if (duplicateSymbols()){
			undefienedSymbols();
			mmap_file(argv[1]);
			undefienedSymbols();
			printf("PASSED\n");
			release_memory();
			return 1;		
		}
	}
	else 
	{
		mmap_file(argv[2]);
		insertSymbIntoArry();
		if (_start_func()){
			if (duplicateSymbols()){
				undefienedSymbols();
				mmap_file(argv[1]);
				undefienedSymbols();
				printf("PASSED\n");
				release_memory();
				return 1;		
			}
		}
	}
	printf("_start check: FAILED\n");
	release_memory();
	exit(-1);

	return 0;
}




