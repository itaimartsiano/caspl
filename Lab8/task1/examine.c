#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <elf.h>
#include <fcntl.h>
#include <sys/mman.h>


Elf32_Ehdr * map_start;

void mmap_file(char * file){

	int fileDesc;
	struct stat fs;
	void * mmap_start;

	if((fileDesc = open(file, O_RDWR)) < 0){
		perror("open file");
		exit(0);
	}

	if (fstat(fileDesc, &fs) != 0){
		perror("fstat");
		exit(0);
	}

	if ((mmap_start=mmap(0, fs.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fileDesc, 0))<0){
		perror("mmap");
		exit(0);
	}

	map_start = (Elf32_Ehdr *) mmap_start;
	return;
}

void print_values(){

	if((map_start->e_ident[1] != 'E') | (map_start->e_ident[2] != 'L') | (map_start->e_ident[3] != 'F'))
	{
		printf("Not an ELF file\n");
		exit(0);
	}

	printf("3 first bytes of the magic number:\t\t\t%d %d %d\n", map_start->e_ident[0], map_start->e_ident[1], map_start->e_ident[2]);
	printf("The data encoding scheme of the object file:\t\t%x\n", map_start->e_ident[5]);
	printf("Entry point:\t\t\t\t\t\t0x%x\n", map_start->e_entry);
	printf("Section header table offset:\t\t\t\tdecimal - %d \n", map_start->e_shoff);
	printf("Number of section header entries:\t\t\t%d\n", map_start->e_shnum);
	printf("Size of section header entry: \t\t\t\t%d\n", map_start->e_shentsize);
	printf("Program offset header table:\t\t\t\tdecimal - %d\n", map_start->e_phoff);
	printf("Number of program header entries:\t\t\t%d\n", map_start->e_phnum);
	printf("Each program header entry size: \t\t\t%d\n", map_start->e_phentsize);

}


void printAllSectionName(){

	int numOfSecHeadears = map_start->e_shnum;
	int i =0;
	
	Elf32_Shdr *  sectionHeader = (Elf32_Shdr *) (((void *)map_start)+(map_start->e_shoff)); 
	Elf32_Shdr * secHeadStrTable = sectionHeader + (map_start->e_shstrndx);	

	const char * const secHeadStrTablePt = ((void *)map_start) + (secHeadStrTable->sh_offset);
	
	printf("Section Headers:\n");
	printf("  [Nr] Name\t\t\tAddr\t\tOff\t\tSize\n");
	
	for(i=0; i<numOfSecHeadears; i++)
	{
		printf("  [%d] %s",i, secHeadStrTablePt+sectionHeader [i].sh_name);
		printf("\t\t\t%08x", sectionHeader [i].sh_addr); 						/*section address*/
		printf("\t %x", sectionHeader [i].sh_offset);						/*section offset*/
		printf("\t\t %x\n", sectionHeader [i].sh_size);						/*section size*/
	}


}

int main(int argc, char ** argv){

	if (argc < 2){
		printf("there is no enougth parameters");
	}
	else{
		mmap_file(argv[1]);
		printAllSectionName();
	}

	return 1;
}




