#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <elf.h>
#include <fcntl.h>
#include <sys/mman.h>


Elf32_Ehdr * map_start;
Elf32_Shdr *  sectionHeader;
Elf32_Shdr * secHeadStrTable;


void mmap_file(char * file){

	int fileDesc;
	struct stat fs;
	void * mmap_start;

	if((fileDesc = open(file, O_RDWR)) < 0){
		perror("open file");
		exit(0);
	}

	if (fstat(fileDesc, &fs) != 0){
		perror("fstat");
		exit(0);
	}

	if ((mmap_start=mmap(0, fs.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fileDesc, 0))<0){
		perror("mmap");
		exit(0);
	}

	map_start = (Elf32_Ehdr *) mmap_start;
	return;
}



void print_values(){

	if((map_start->e_ident[1] != 'E') | (map_start->e_ident[2] != 'L') | (map_start->e_ident[3] != 'F'))
	{
		printf("Not an ELF file\n");
		exit(0);
	}

	printf("3 first bytes of the magic number:\t\t\t%d %d %d\n", map_start->e_ident[0], map_start->e_ident[1], map_start->e_ident[2]);
	printf("The data encoding scheme of the object file:\t\t%x\n", map_start->e_ident[5]);
	printf("Entry point:\t\t\t\t\t\t0x%x\n", map_start->e_entry);
	printf("Section header table offset:\t\t\t\tdecimal - %d \n", map_start->e_shoff);
	printf("Number of section header entries:\t\t\t%d\n", map_start->e_shnum);
	printf("Size of section header entry: \t\t\t\t%d\n", map_start->e_shentsize);
	printf("Program offset header table:\t\t\t\tdecimal - %d\n", map_start->e_phoff);
	printf("Number of program header entries:\t\t\t%d\n", map_start->e_phnum);
	printf("Each program header entry size: \t\t\t%d\n", map_start->e_phentsize);

}


void printAllSectionName(){

	int numOfSecHeadears = map_start->e_shnum;
	int i =0;
	
	sectionHeader = (Elf32_Shdr *) (((void *)map_start)+(map_start->e_shoff)); 
	secHeadStrTable = sectionHeader + (map_start->e_shstrndx);	

	const char * const secHeadStrTablePt = ((void *)map_start) + (secHeadStrTable->sh_offset);
	
	printf("Section Headers:\n");
	printf("  [Nr] Name\t\t\tAddr\t\tOff\t\tSize\n");
	
	for(i=0; i<numOfSecHeadears; i++)
	{
		printf("  [%d] %s",i, secHeadStrTablePt+sectionHeader [i].sh_name);
		printf("\t\t\t%08x", sectionHeader [i].sh_addr); 						/*section address*/
		printf("\t %x", sectionHeader [i].sh_offset);						/*section offset*/
		printf("\t\t %x\n", sectionHeader [i].sh_size);						/*section size*/
	}
}



void printAllSymbolTable(){

	int numOfSecHeadears = map_start->e_shnum;
	sectionHeader = (Elf32_Shdr *) (((void *)map_start)+(map_start->e_shoff)); 
	secHeadStrTable = sectionHeader + (map_start->e_shstrndx);	
	const char * const secHeadStrTablePt = ((void *)map_start) + (secHeadStrTable->sh_offset);

	const char * const strtab = ((void *)map_start) + ((secHeadStrTable+2)->sh_offset);
	
	int offset = ((secHeadStrTable+1)->sh_offset);
	Elf32_Sym * symTable = (Elf32_Sym *) (((void *)map_start) + offset);
	
	int numOfSecHeadears = map_start->e_shnum;
	int sizeOfSymTable = sectionHeader [numOfSecHeadears-2].sh_size;
	int numOfSymbols = sizeOfSymTable/sizeof(Elf32_Sym);
	int i =0;
	
	printf("Symbol table:\n");
	printf("  Num:\tValue\t\tNdx\t\tSection name\t\tSymbol name\n");
	
	for(i=0; i<numOfSymbols; i++)
	{
		int secIndex = symTable[i].st_shndx;
		printf("  %d:",i);
		printf("\t%08x", symTable[i].st_value); 						
		printf("\t%d", secIndex);	
	
		if (secIndex>0 && secIndex<numOfSecHeadears){
			printf("\t\t%s", secHeadStrTablePt+sectionHeader[secIndex].sh_name);	
		}
		else
			if (secIndex == 0) printf("\t\tUND");
			else printf("\t\tABS");
	
		printf("\t\t\t%s\n", strtab+(symTable[i].st_name));						
	}


}


int main(int argc, char ** argv){

	if (argc < 2){
		printf("there is no enougth parameters");
	}
	else{
		mmap_file(argv[1]);
		printAllSymbolTable();
	}

	return 1;
}




