#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "LineParser.h"

#define PATH_MAX 200
#define USER_COMMAND_LEN 2048

extern int waitpid();


typedef struct HistoryList
{
    char command [USER_COMMAND_LEN];
    struct HistoryList *next;
} HistoryList;



HistoryList * enterNewObject(HistoryList * head, char * user_command){
	HistoryList * newHead = malloc(sizeof(HistoryList));
	strcpy(newHead->command, user_command);
	newHead->next = 0;
	head->next = newHead;
	return newHead;
}



void freeHistoryList(HistoryList * tail){
	HistoryList * curr = tail->next;
	while (curr != 0){
		HistoryList * next = curr->next;
		free(curr);
		curr = next;
	}
	free(tail);
}

void printHistoryList(HistoryList * tail){
	HistoryList * curr = tail->next;
	int i = 0;
	while (curr != 0){
		HistoryList * next = curr->next;
		printf("#%d %s", i,curr->command);
		curr = next;
		i++;
	}
}


int execute(cmdLine * pCmdLine, HistoryList * tail){

	if (pCmdLine){

  		if (strncmp("cd" , pCmdLine->arguments[0], 2) == 0){
			int status = chdir(pCmdLine->arguments[1]);
			if (status<0) perror("Directory Error");
			return (status);
		}

		if (strncmp("history" , pCmdLine->arguments[0], 7) == 0){
			printHistoryList(tail);
			return (0);
		}

		int child_pid = 0;
  		int child_status = 0;
		child_pid = fork();

  		if(child_pid == 0) {

    		execvp(pCmdLine->arguments[0], pCmdLine->arguments);
   
    		perror("Error message");
    		freeHistoryList(tail);
    		freeCmdLines(pCmdLine);
    		exit(0);

  		}
  		else {
  			if (pCmdLine->blocking){
  				waitpid(child_pid,&child_status,0);	
  			}
  			
     		return child_status;
 	 	}

	}
	
	return (0);

}


int main (int argc , char* argv[], char* envp[])
{
	cmdLine * parsed_line;
	HistoryList * tail = malloc(sizeof(HistoryList));
	HistoryList * head = tail;

	while (1){
		
		char curr_directory [PATH_MAX];
		getcwd(curr_directory,PATH_MAX-1);
		printf("%s>",curr_directory);

		char user_command [USER_COMMAND_LEN+1];
		fgets (user_command, USER_COMMAND_LEN, stdin);
		
		if (strlen(user_command) >1){
		head = enterNewObject(head,user_command);	
		}
		

		if (strncmp("quit",user_command,4) == 0){
			freeHistoryList(tail);
			exit(0);
		}	
		
		parsed_line = parseCmdLines(user_command);
		execute(parsed_line, tail);
		freeCmdLines(parsed_line);
	}

  	return 0;
}
