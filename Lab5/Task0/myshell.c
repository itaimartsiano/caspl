#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "LineParser.h"

#define PATH_MAX 200
#define USER_COMMAND_LEN 2048

/*extern char * getcwd(char * buf,int pathsize);*/


int execute(cmdLine * pCmdLine){

	int ans =0;

	if (pCmdLine){

		int child_pid;
  		int child_status;
		child_pid = fork();
  		if(child_pid == 0) {
    	/* This is done by the child process. */

    		execvp(pCmdLine->arguments[0], pCmdLine->arguments);
    
    		/* If execv returns, it must have failed. */
    		char * error_message = "Error message";
    		perror(error_message);
    		exit(0);
  		}
  		else {
     		/* This is run by the parent.  Wait for the child to terminate. */

     		wait(&child_status);

     		return child_status;
 	 	}


	}
	
	return ans;

}


int main (int argc , char* argv[], char* envp[])
{
	cmdLine * parsed_line;

	while (1){
		
		char curr_directory [PATH_MAX];
		getcwd(curr_directory,PATH_MAX-1);
		printf("%s\n",curr_directory);

		char user_command [USER_COMMAND_LEN+1];
		fgets (user_command, USER_COMMAND_LEN, stdin);

		if (strncmp("quit",user_command,4) == 0){
			exit(0);
		}	
		
		parsed_line = parseCmdLines(user_command);
		execute(parsed_line);
		freeCmdLines(parsed_line);
	}

	/*there is leak ---------*/

  	return 0;
}
