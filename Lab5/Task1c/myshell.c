#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "LineParser.h"

#define PATH_MAX 200
#define USER_COMMAND_LEN 2048

extern int waitpid();

int execute(cmdLine * pCmdLine){

	if (pCmdLine){

  		if (strncmp("cd" , pCmdLine->arguments[0], 2) == 0){
			int status = chdir(pCmdLine->arguments[1]);
			if (status<0) perror("Directory Error");
			return (status);
		}

		int child_pid = 0;
  		int child_status = 0;
		child_pid = fork();

  		if(child_pid == 0) {

    		execvp(pCmdLine->arguments[0], pCmdLine->arguments);
   
    		char * error_message = "Error message";
    		perror(error_message);
    		freeCmdLines(pCmdLine);
    		exit(0);
  		}
  		else {
  			if (pCmdLine->blocking){
  				waitpid(child_pid,&child_status,0);	
  			}
  			

     		return child_status;
 	 	}

	}
	
	return (0);

}


int main (int argc , char* argv[], char* envp[])
{
	cmdLine * parsed_line;

	while (1){
		
		char curr_directory [PATH_MAX];
		getcwd(curr_directory,PATH_MAX-1);
		printf("%s>",curr_directory);

		char user_command [USER_COMMAND_LEN+1];
		fgets (user_command, USER_COMMAND_LEN, stdin);

		if (strncmp("quit",user_command,4) == 0){
			exit(0);
		}	
		
		parsed_line = parseCmdLines(user_command);
		execute(parsed_line);
		freeCmdLines(parsed_line);
	}

  	return 0;
}
