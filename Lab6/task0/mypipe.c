#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "LineParser.h"



int main(int argc, char ** argv)
{
		int status;
		char* input;
		int pipefd[2];

		if(pipe(pipefd) < 0)
			perror("Pipe error:");
			
		int child_pid = fork();
		
		if(child_pid==0)
		{
			write(pipefd[1], "hello", 5);
		}
		else if(child_pid <0)
		{
			perror("Fork error:");
		}
		else{
			input = (char*)malloc(sizeof(char)*20);
			waitpid(child_pid, &status, 0);

			if(read(pipefd[0], input, 5) == 5)
				printf("%s\n", input);
			else
				perror("Read error:");
			free(input);
		}


	
	return 0;
}


