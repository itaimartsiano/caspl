#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>


int main(int argc, char ** argv)
{
	int child_pid1, child_pid2;
	int status;
	int pipefd[2];

	char * const ls[] = {"ls","-l" ,"/usr/bin",0};
	char * const tail[] = {"tail", "-n", "2", 0};

	if(pipe(pipefd) == -1)	perror("Pipe error:");
	
	child_pid1 = fork();
	
	if (child_pid1 == -1) perror("Fork error:");
	
	if(child_pid1==0)
	{
		close(1);
		dup(pipefd[1]);
		close(pipefd[1]);
		execvp(ls[0], ls);
	}
	else{
		close(pipefd[1]);
		child_pid2 = fork();
		
		if(child_pid2 == -1)	perror("Fork error:");

		if(child_pid2==0)
		{
			close(0);
			dup(pipefd[0]);
			close(pipefd[0]);
			execvp(tail[0], tail);
		}
		else 
		{
			close(pipefd[0]);
			waitpid(child_pid1, &status, 0);
			waitpid(child_pid2, &status, 0);
		}

	}

	return 0;
}


