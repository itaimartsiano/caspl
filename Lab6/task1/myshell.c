#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include "LineParser.h"


#define PATH_MAX 200
#define USER_COMMAND_LEN 2048

extern int waitpid();

typedef struct LinkedList
{
    char * name;
    char * value;
    struct LinkedList *next;
} LinkedList;


void addVariable(LinkedList * list, char * name, char * value ){
	
	LinkedList * curr = list;
	
	while (curr->next){
		if (curr->name && strncmp(curr->name,name, strlen(name))==0){
			curr->value = realloc(curr->value, strlen(value)+1);
			strcpy(curr->value,value);

			return;
		}
		curr = curr->next;
	}

	if (curr->name && strncmp(curr->name,name, strlen(name))==0){
			curr->value = realloc(curr->value, strlen(value)+1);
			strcpy(curr->value,value);
			return;
	}else{
		
		LinkedList * link = malloc(sizeof(LinkedList));
		link->name = malloc(strlen(name)+1);
		link->value = malloc(strlen(value)+1);
		link->next=0;
		strcpy(link->name, name);
		strcpy(link->value, value);
		curr->next = link;
		return;
	}


}


void freeLinkedList(LinkedList * list){
	LinkedList * curr = list->next;
	while (curr != 0){
		LinkedList * next = curr->next;
		free(curr->name);
		free(curr->value);
		free(curr);
		curr = next;
	}
	free(list);
}

void env(LinkedList * list){
	
	LinkedList * curr = list->next;

	while (curr != 0){
		LinkedList * next = curr->next;
		printf("%s = %s\n", curr->name, curr->value);
		curr = next;
	
	}
}

char * findSign(char * sign, LinkedList * list){
	
	
	while (list){
		if (list->name && (!strcmp(list->name, sign))){
			return list->value;
		}
		list = list->next;
	}

	printf("The internal variable %sis not exist\n",sign);
	return 0;
}


void renameLink(LinkedList * list, char * name, char * new_name){
	
	LinkedList * curr = list;
	while (curr){
		if (curr->name && strncmp(curr->name,new_name, strlen(new_name))==0){
			printf("The name is already exist\n");
			return;
		}
		curr = curr->next;
	}

	while (list){
		if (list->name && strncmp(list->name,name, strlen(name))==0){
			list->name = realloc(list->name, strlen(new_name)+1);
			strcpy(list->name,new_name);
			return;
		}
		list = list->next;
	}
	printf("The requested name is not exist\n");
	return;
}



void deleteLink(LinkedList * list, char * name){

	LinkedList * prev = list;
	while (list){
		if (list->name && strncmp(list->name,name, strlen(name))==0){
			free(list->name);
			free(list->value);
			prev->next = list->next;
			free(list);
			return;
		}
		prev = list;
		list = list->next;
	}

	printf("The requested name to delete is not exist\n");
	return;
}



typedef struct HistoryList
{
    char command [USER_COMMAND_LEN];
    struct HistoryList *next;
} HistoryList;



HistoryList * enterNewObject(HistoryList * head, char * user_command){
	HistoryList * newHead = malloc(sizeof(HistoryList));
	strcpy(newHead->command, user_command);
	newHead->next = 0;
	head->next = newHead;
	return newHead;
}



void freeHistoryList(HistoryList * tail){
	HistoryList * curr = tail->next;
	while (curr != 0){
		HistoryList * next = curr->next;
		free(curr);
		curr = next;
	}
	free(tail);
}

void printHistoryList(HistoryList * tail){
	HistoryList * curr = tail->next;
	int i = 0;
	while (curr != 0){
		HistoryList * next = curr->next;
		printf("#%d %s\n", i,curr->command);
		curr = next;
		i++;
	}
}


int execute(cmdLine * pCmdLine, HistoryList * tail, LinkedList * intern_variables){

	if (pCmdLine){

  		if (strncmp("cd" , pCmdLine->arguments[0], 2) == 0){
			int status = chdir(pCmdLine->arguments[1]);
			if (status<0) perror("");
			return (status);
		}

		if (strncmp("history" , pCmdLine->arguments[0], 7) == 0){
			printHistoryList(tail);
			return (0);
		}

		if (strncmp("set" , pCmdLine->arguments[0], 3) == 0 && (pCmdLine->argCount==3)){
			addVariable(intern_variables, pCmdLine->arguments[1], pCmdLine->arguments[2]);
			return (0);
		}

		if (strncmp("env" , pCmdLine->arguments[0], 3) == 0){
			env(intern_variables);
			return (0);
		}

		if (strncmp("delete" , pCmdLine->arguments[0], 6) == 0){
			deleteLink(intern_variables, pCmdLine->arguments[1]);
			return (0);
		}


		if (strncmp("rename" , pCmdLine->arguments[0], 6) == 0){
			renameLink(intern_variables, pCmdLine->arguments[1], pCmdLine->arguments[2]);
			return (0);
		}

		int child_pid = 0;
  		int child_status = 0;
		child_pid = fork();

  		if(child_pid == 0) {
  			
  			if(pCmdLine->outputRedirect!=NULL)	{
  				close(1);
				if (open(pCmdLine->outputRedirect, O_WRONLY)<0){
					perror("open error:");
				}
				
  			}

			if(pCmdLine->inputRedirect!=NULL){
				close(0);
				if (open(pCmdLine->inputRedirect, O_RDONLY)<0){
					perror("open error:");
				}
			}

    		execvp(pCmdLine->arguments[0], pCmdLine->arguments);

    		if(pCmdLine->inputRedirect!=NULL)	close(0);
    		if(pCmdLine->outputRedirect!=NULL)	close(1);
   
    		perror("Error message");
    		freeHistoryList(tail);
    		freeCmdLines(pCmdLine);
    		freeLinkedList(intern_variables);
    		exit(0);

  		}
  		else {
  			if (pCmdLine->blocking){
  				waitpid(child_pid,&child_status,0);	
  			}
  			
     		return child_status;
 	 	}

	}
	
	return (0);

}


int main (int argc , char* argv[], char* envp[])
{
	cmdLine * parsed_line;
	HistoryList * tail = malloc(sizeof(HistoryList));
	HistoryList * head = tail;
	LinkedList * intern_variables = malloc(sizeof(LinkedList));
	intern_variables->next=0;
	intern_variables->name=0;

	while (1){
		
		char curr_directory [PATH_MAX];
		getcwd(curr_directory,PATH_MAX-1);
		printf("%s>",curr_directory);

		char user_command [USER_COMMAND_LEN+1];
		fgets (user_command, USER_COMMAND_LEN, stdin);
		user_command[strlen(user_command)-1] = 0;

		
		if (strncmp("!" , user_command, 1) == 0){

			int command_line = atoi((user_command)+1);
			HistoryList * curr = tail->next;
			int i = 0;

			while (curr != 0 && i<command_line){
				curr = curr->next;
				i++;
			}

			if(i==command_line && curr != 0){
				strcpy(user_command ,curr->command);
			}
			else{
				printf("Non exisiting command\n");
			}	

		}

		parsed_line = parseCmdLines(user_command);
		int j=0;
		
		for (j=0; j<parsed_line->argCount; j++){
			char * pos = 0;
			pos = strchr(parsed_line->arguments[j], '$');
			if (pos){
				char * value = findSign(pos+1, intern_variables);
					
				if (value){
					replaceCmdArg(parsed_line, j, value);

				} 
			}

		}

		char user_commandH [USER_COMMAND_LEN+1];
		user_commandH[0] = 0;
		int length = 0;
		for (j=0; j<parsed_line->argCount; j++){
			length = strlen(user_commandH);
			strcpy(user_commandH + length, parsed_line->arguments[j]);
			user_commandH[length + strlen(parsed_line->arguments[j])] = ' ';
			user_commandH[length + strlen(parsed_line->arguments[j])+1] = 0;
			int numOfParam = strlen(user_commandH);
			if (j==numOfParam && parsed_line->inputRedirect != NULL){
				length = strlen(user_commandH);
				strcpy(user_commandH + length, "< ");
				strcpy(user_commandH + length + 2, parsed_line->inputRedirect);
				user_commandH[length+2+ strlen(parsed_line->inputRedirect)] = ' ';
				user_commandH[length+3+ strlen(parsed_line->inputRedirect)] = 0;
			}
			if (j==numOfParam && parsed_line->outputRedirect != NULL){
				length = strlen(user_commandH);
				strcpy(user_commandH + length, "> ");
				strcpy(user_commandH + length + 2, parsed_line->outputRedirect);
				user_commandH[length+2+ strlen(parsed_line->outputRedirect)] = ' ';
				user_commandH[length+3+ strlen(parsed_line->outputRedirect)] = 0;
			}
		}

		if (strlen(user_commandH) >1){
			head = enterNewObject(head,user_commandH);	
		}

		if (strncmp("quit",user_command,4) == 0){
			freeHistoryList(tail);
			freeLinkedList(intern_variables);
			freeCmdLines(parsed_line);
			exit(0);
		}	
		
		
		execute(parsed_line, tail, intern_variables);
		freeCmdLines(parsed_line);
	}

  	return 0;
}
