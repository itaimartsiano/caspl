#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define BUFFERSIZE 4096



void mem_dispaly(char * filename, int size, void * mem_buffer){
	
	return ;
}



void load_into_mem(char * filename, int size, void * mem_buffer){

	return;
}



void save_into_file(char * filename, int size, void * mem_buffer){
	return;
}



void quit(char * filename, int size, void * mem_buffer){
  free(mem_buffer);
  exit(0);
}


struct fun_desc {
  char *name;
  void (*fun)(char * filename, int size, void * mem_buffer);
};




int main(int argc, char **argv){

	char * mem_buffer = (char *) malloc(BUFFERSIZE);
	char * filename;
	int size = 1;
	char command [50];

	if (argc == 2)	filename = argv[1];
	else
		if (argc == 3){
			filename = argv[1] ;
			size = *argv[2];
			size = size - 48;
			if (size != 1 && size != 2 && size != 4){
				printf("wrong size number\n");
				free(mem_buffer);
				exit(0);
			}
		}
		else
		{
			printf("wrong input values\n");
			free(mem_buffer);
			exit(0);
		}

	FILE * file = 0;
	file = fopen(filename, "r+");
	if (!file) {
		free(mem_buffer);
		perror("fopen");
		exit(0);
	}
	fclose (file);


	struct fun_desc menu[] = { { "Mem Display", mem_dispaly }, { "Load Into Memory", load_into_mem }, 
				   { "Save Into File", save_into_file }, { "Quit", quit}, { NULL, NULL } };
	
	
	while (1){
		
		int i=0;
		int num=0;
		printf("File: %s, buffer location: %p, choose action:\n", filename, mem_buffer);
		 while (menu[i].name != 0){
		  	printf("%d) %s \n", i, menu[i].name);
		 	i++;
	     }
	     printf("Option: ");

	 	fgets(command, 50, stdin);
		num = atoi(command);
		 
		 if (num<i && num >=0){
		    (menu[num].fun)(filename, size,mem_buffer);
		    printf("DONE.\n");
		 }

		  printf("\n\n");


	}
	
	return 0;
}


















