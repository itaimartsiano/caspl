#include <stdio.h>
extern char * array_element_address(char * Array_start, int Element_size, int Array_width, int x, int y);


extern int WorldWidth, WorldLength, element_size;
extern char state;
extern void resume();


/*this function return 1 if cell is alive and else return 0*/

int is_alive(char *  cell){

	if (*(cell) != ' '){
		return 1;
	}else
	{
		return 0;
	}

}



/*the function run in loop, check num of live neighbours cells, resume update the age or kill cell, 
resume and loop again */

void cell (int x, int y){
	
	char * cell = array_element_address(&state, 1, WorldWidth, x, y);
	
	while(1){
		int cell_status = is_alive(cell);
		int live_cells = 0;
		
		char * check_cell = array_element_address(&state, 1, WorldWidth, (x-1+WorldWidth)%WorldWidth, y);
		if (is_alive(check_cell)) live_cells ++;

		check_cell = array_element_address(&state, 1, WorldWidth, (x-1+WorldWidth)%WorldWidth, (y-1+WorldLength)%WorldLength);
		if (is_alive(check_cell)) live_cells ++;

		check_cell = array_element_address(&state, 1, WorldWidth, x, (y-1+WorldLength)%WorldLength);
		if (is_alive(check_cell)) live_cells ++;

		check_cell = array_element_address(&state, 1, WorldWidth, (x+1)%WorldWidth, (y-1+WorldLength)%WorldLength);
		if (is_alive(check_cell)) live_cells ++;

		check_cell = array_element_address(&state, 1, WorldWidth, (x+1)%WorldWidth, y);
		if (is_alive(check_cell)) live_cells ++;

		check_cell = array_element_address(&state, 1, WorldWidth, (x+1)%WorldWidth, (y+1)%WorldLength);
		if (is_alive(check_cell)) live_cells ++;

		check_cell = array_element_address(&state, 1, WorldWidth, x, (y+1)%WorldLength);
		if (is_alive(check_cell)) live_cells ++;

		check_cell = array_element_address(&state, 1, WorldWidth, (x-1+WorldWidth)%WorldWidth, (y+1)%WorldLength);
		if (is_alive(check_cell)) live_cells ++;

		__asm__("xor %ebx,%ebx");
		resume();

		if (cell_status && (live_cells == 2 || live_cells ==3)){					/*if curr cell is suppose to live in next generation*/
			if ((*cell) != '9'){
				(*cell) = (*cell) + 1;
			}
		}else{
			if (cell_status){														/*cell is alive but need to die in the next generation*/
				(*cell) = ' ';
			}else{																	/*cell is dead maybe need to live*/
				if (live_cells==3){
					(*cell) = '1';
				}
			}
		}

		__asm__("xor %ebx,%ebx");
		resume();

	}
}
