
global scheduler
extern resume, end_co, T, K, element_size, printf

section .data
	cors_size:dd 0
	count_when_to_print: dd 0
	tmp_index: dd 0


section .text
										
scheduler:
		mov eax, [element_size]				;size of matrix
		mov [cors_size], eax				;put the size of matrix into coroutine size param
		add dword [cors_size], 2			;add 2 to corotuines because of printer and sceduler
		mov ebx, 2							;ebx use to count num of corotines had done
		mov ecx, [T]
		shl ecx, 1
		mov [T], ecx
		
	.next:
		cmp dword [T], 0					;check if the number of generations to do is 0
		je .end

		mov eax, [K]
		cmp eax, [count_when_to_print]		;check if resumes counter equal to K param
		jne .cont_coroutine					
		
	;----printing call----
		mov [tmp_index], ebx			
		mov ebx, 1
		call resume 						
		mov dword [count_when_to_print], 0	;reset the K counter
		mov ebx, [tmp_index]				;restore the corotines index counter (ebx)
	
	.cont_coroutine:
	    
	    call resume       					;call the coroute func
		
	;----advance counters	   
		inc ebx
		inc dword [count_when_to_print]
	
		cmp ebx, [cors_size]				;check if had done one generation (one generation is one cycle over all the corot)
		jne .next

		dec dword [T]						;decrease number of generations
		mov ebx, 2
	    jmp .next

	.end:									
		mov ebx, 1							;call printer for the last time
		call resume
	    call end_co             			;stop coroute
