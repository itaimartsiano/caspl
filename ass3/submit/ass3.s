%macro startFunc 0						;macro collect all the start function commands
	push	ebp
	mov	ebp, esp	; entry code
	pushad		; Save registers
%endmacro


%macro endFunc 0						;macro collect all the end void function commands
	popad								;Restore registers
	mov	esp, ebp						;Function exit code
	pop	ebp
	ret
%endmacro	

%macro print_string 1					;macro to print strrings to screen
	jmp %%bla
	%%str: DB %1 , 10,0
	%%bla:
	pushf
	pusha
	push %%str
	call printf
	add esp,4
	popa
	popf
%endmacro

%macro calculate_place_state 0
	mov eax, [WorldWidth]				;start to calculate
	mul esi
	mov ecx, state
	add ecx, eax
	add ecx, edi 					;end to calculate which place to use
%endmacro

%macro print_bytes 2
	mov ecx, %2							;put in ecx the place we are in state
	mov ebx,1										;file descriptor (stdout)
	mov edx, %1	 									;message length
	mov eax,4 										;system call number (sys_write)	
	int 0x80 										;call kernel
%endmacro

%macro read_from_file_macro 2
	mov ecx, %2				;put in ecx the place we are in state
    mov eax, 3						; system call number (sys_read)
	mov ebx, %1				; file descriptor
	mov edx, 1						; bytes to read
	int 0x80 						;call kernel
%endmacro


global main
extern init_co, start_co, resume, atoi, printf
extern scheduler, printer, print_matrix, cell, init_cell
global array_element_address
global state
global WorldLength
global WorldWidth
global element_size, T, K, pointer_to_state


 ;;/usr/include/asm/unistd_32.h
sys_exit:       equ   1
MATRIX_SIZE EQU 60*60+2
ARG_V EQU 12
ARG_1 EQU 4
ARG_2 EQU 8
ARG_3 EQU 12
ARG_4 EQU 16
ARG_5 EQU 20

;-----------------------------------------------------------------------------


section .rodata
	format_pointer: db "%p" ,10
	format_number: db "%d" ,10
	format_string: db "String is: %c",10
	
	


section .data
	state times MATRIX_SIZE db ' '					;,matrix 60*60
	WorldLength: DD 0
	WorldWidth: DD 0
	T: DD 0
	K: DD 0
	element_size: DD 0
	fileDS:   DD 0
	pointer_to_state: DD 0
	tmp_pointer: dd 0
	

section .text


main:
    enter 0, 0

	mov eax, [ebp+8]				;eax = argc
	cmp eax, 6
	jl .error_number_of_param

;---get param from user to read init file into memory

	mov eax, dword [ebp+ARG_V]			;assume all the values are ok
	mov ebx, dword[eax+ARG_1]			;get file name
	push ebx							;push file name into stack
	
	mov ebx, dword [eax+ARG_2]			;get file width
	push ebx
	call atoi							;change from string to int
	add esp, 4
	
	push eax							;push length into stack
	mov eax, dword [ebp+ARG_V]
	mov ebx, dword [eax+ARG_3]			;get file length
	push ebx
	call atoi
	add esp, 4 
	
	push eax							;push WorldWidth into stack
	call read_from_file					;read the file into state (matrix)
	add esp, 12							;restore the stack pointer state

	mov eax, dword [ebp+ARG_V]
	mov ebx, [eax+ARG_4]				;get number of generations

	push ebx
	call atoi							;translate char to int
	add esp, 4 
	
	mov [T], eax						;insert num of generations into T

	mov eax, dword [ebp+ARG_V]
	mov ebx, [eax+ARG_5]				;get number of K resumes

	push ebx
	call atoi							;translate char to int
	add esp, 4 

	mov [K], eax

;---end to read param from user
;---initialization of coroutines

    xor ebx, ebx            			; scheduler is co-routine 0
    mov edx, scheduler
    call init_co            			; initialize scheduler state

    inc ebx                 			; printer is co-routine 1
    mov edx, printer
    call init_co            			; initialize printer state

    xor esi, esi 						; y value
    xor edi, edi 						; x value
    inc ebx
    mov edx, cell 						; function to coroutine

.coroutines_cells_init_loop:
	call init_cell
	inc ebx
	inc edi
	cmp edi, [WorldWidth]
	jne .coroutines_cells_init_loop		;next init the next routine
	
.line_up:
	inc esi								;have to down line 
	xor edi,edi							
	cmp esi, dword [WorldLength] 
	je .done_init
	jmp .coroutines_cells_init_loop

;---done initialization, start game

.done_init:
    xor ebx, ebx            			; starting co-routine = scheduler
    call start_co           			; start co-routines


.exit:
    mov eax, sys_exit
    xor ebx, ebx
    int 80h

.error_number_of_param:
	print_string "Error: number of param not enougth"
	jmp .exit








;----------------------------------------------------------------------------

read_from_file:
	startFunc

;---open file

    mov     eax, 5         				;sys_open function
    mov     ebx, [ebp+16]    			;path of file
    mov     ecx, 0       				;open for reading
    int     0x80            			;Transfer control to operating system

    cmp eax, 0
    jl .erorr_open_file              	;handle this option
    mov [fileDS],eax

;---calculate the matrix size
    mov esi, [ebp+12]					;length
    mov edi, [ebp+8]					;WorldWidth
    mov [WorldLength], esi
    mov [WorldWidth], edi
    mov eax, esi
    mul edi
    mov [element_size], eax				;number of cells in matrix
    
;---read from the file byte after byte
    xor esi, esi
    xor edi, edi
    mov [pointer_to_state], dword state 				;put matrix into pointer


.byte_from_file_to_matrix:

    read_from_file_macro [fileDS], [pointer_to_state]
	inc edi
	inc dword [pointer_to_state]
	cmp edi, [WorldWidth]
	jne .byte_from_file_to_matrix						;need to count up the line
	
.line_up:
	
	read_from_file_macro [fileDS], [pointer_to_state]
	cmp eax, 0
	je .label_end_to_read_from_file
	mov edx, [pointer_to_state]
	cmp byte [edx], ' '
	je .cont
	cmp byte [edx], '1'
	je .cont
	jmp .line_up
.cont:
	inc esi												;inc line in matrix
	xor edi, edi
	inc edi
	inc dword [pointer_to_state]						
	cmp esi, dword [WorldLength] 
	je .label_end_to_read_from_file
	jmp .byte_from_file_to_matrix


.erorr_open_file:
	print_string "Error: cannot open file"
	jmp .exit	

.label_end_to_read_from_file:
	
	mov byte [pointer_to_state], 10
	mov byte [pointer_to_state+1], 0
	mov ebx, [fileDS]									;close file
	mov eax, 6
	int 0x80											;call operating system to perform
 	endFunc

 .exit:
    mov eax, sys_exit
    xor ebx, ebx
    int 80h







;------------------------------------------------------------------------

array_element_address:
	startFunc
	

	mov ecx, [ebp+20] 					;x
	mov eax, [ebp+24] 					;y	
					
	mul dword [ebp+16] 					;line number * width
	mul dword [ebp+12]					;multiply in elements size

	add eax, ecx						;add the current location in line
	add eax, [ebp+8]					;add state adress to relative position
	
	mov [tmp_pointer], eax
	popad
	
	mov eax, [tmp_pointer]
	mov esp, ebp
	pop ebp
	ret



;------------------------------------------------------------------------

