global printer, print_matrix
extern resume
extern WorldLength, WorldWidth, pointer_to_state, state



        ;; /usr/include/asm/unistd_32.h
sys_write:      equ   4
stdout:         equ   1
%macro startFunc 0						;macro collect all the start function commands
	push	ebp
	mov	ebp, esp	; entry code
	pushad		; Save registers
%endmacro


%macro endFunc 0						;macro collect all the end void function commands
	popad								;Restore registers
	mov	esp, ebp						;Function exit code
	pop	ebp
	ret
%endmacro	


%macro print_bytes 2
	mov ecx, %2										;put in ecx the place we are in state
	mov ebx, stdout									;file descriptor (stdout)
	mov edx, %1	 									;message length
	mov eax,4 										;system call number (sys_write)	
	int 0x80 										;call kernel
%endmacro



section .data
newLine:  dd 10										;char for new line


section .text
printer:
        call print_matrix

        xor ebx, ebx								;reset ebx to 0
        call resume             					;resume scheduler

        jmp printer

;------------------------------------------------------------------------
print_matrix:
	startFunc
	xor esi, esi									;WorldLength
	mov [pointer_to_state], dword state 			;put matrix start point into pointer


.print_line_from_matrix:
	print_bytes [WorldWidth], [pointer_to_state]	;use macro print bytes to print one line start from pointer
	print_bytes 1, newLine							;print new line
	
;---advance the pointer to next line--------
	mov  eax, [pointer_to_state]
	add eax, [WorldWidth]
	mov [pointer_to_state], eax

	inc esi
	cmp esi, dword [WorldLength] 
	jne .print_line_from_matrix						;not end of lines, back to print one more line

	endFunc


