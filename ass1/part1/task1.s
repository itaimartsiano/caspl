section	.rodata
LC0:
	DB	"The result is:  %s", 10, 0	; Format string

section .bss
LC1:
	RESB	32

section .text
	align 16
	global my_func
	extern printf

my_func:
	push	ebp
	mov	ebp, esp				; Entry code - set up ebp and esp
	pusha						; Save registers

	mov ecx, dword [ebp+8]		; Get argument (pointer to string)

								; edi - counter to pointer of answer
	mov edx , 0					;temporary number to save average sum
	mov esi , 0 				;counter of modolu 4
	mov eax, 1 					;count the number of charcters in input
	
	cmp byte [ecx] , 10			;check if input is empty
	je label_end

	moveRight:					;move the pointer of input to the least significant digit
		inc ecx					;enhance the pointer
		inc eax					
		cmp byte [ecx] , 10		;check if we arrive the end of the input
		je loop
		jmp moveRight



	loop:						;run in loop on char from LSB to MSB, every 4 char are section
		
		dec eax
		cmp eax , 0
		je reset				;if we run over all the char in input jump to reset function

		dec ecx					;move one char left

		cmp esi , 0 
		je loop0
		
		cmp esi , 1
		je loop1
		
		cmp esi , 2
		je loop2

		cmp esi , 3
		je loop3
    
    loop0:						;in the first number in section
    	
    	cmp byte [ecx] , 48
    	je is_0 

  		add edx ,1
    	inc esi
    	cmp esi , 4
    	je reset
    	jmp loop

    loop1:						;in the second number in section
  
    	cmp byte [ecx] , 48
    	je is_0 

  		add edx ,2
    	inc esi
    	cmp esi , 4
    	je reset
    	jmp loop

    loop2:						;in the third number in section
    
    	cmp byte [ecx] , 48
    	je is_0 

    	add edx , 4
    	inc esi
    	cmp esi , 4
    	je reset
    	jmp loop

    loop3:						;in the fourth number in section

    	cmp byte [ecx] , 48
    	je is_0 

  		add edx ,8
    	inc esi
    	cmp esi , 4
    	je reset
    	jmp loop


    is_0:						;if the char is 0
    	inc esi
    	cmp esi , 4
    	je reset
    	jmp loop
    	

    reset:						;enter the number in hex to the answer, and go back to loop or label end
    	cmp edx , 10
    	jae above
    	jb under

    	above:
    		add edx , 55
    		jmp continue
    	
    	under:
    		add edx , 48
    		jmp continue

    continue:
    	mov byte [LC1+edi] , dl
    	mov edx , 0
    	inc edi					;increment the number of char in answer
    	mov esi , 0
		cmp eax , 0
		je label_end
    	jmp loop
    


	label_end:					;we arrive here only after end to run all over the characters

		add edi , LC1
		mov eax , edi			;point to the end of answer
		dec eax
		mov ebx , LC1			;point to start of the answer

	start:						
		cmp ebx , eax
		jb reverse
		jmp print

	reverse:					;change characters of eax and ebx
		mov cl , byte [ebx]
		mov dl , byte [eax]
		mov byte [ebx] , dl
		mov byte [eax] , cl
		inc ebx
		dec eax
		jmp start
	
	print:						
		push	LC1				; Call printf with 2 arguments: pointer to str
		push	LC0				; and pointer to format string.
		call	printf
		add 	esp, 8			; Clean up stack after call

		popa					; Restore registers
		mov	esp, ebp			; Function exit code
		pop	ebp
		ret

