section	.rodata
LC0:
	DB	"x is out of range", 10, 0	; Format string
LC1:
	DB	"%d", 10, 0	; Format integer

section .bss

section .text
	align 16
	global calc_func
	extern printf					;declare external function
	extern check					;declare external function

calc_func:
	push	ebp						;push the basic pointer 
	mov	ebp, esp					;Entry code - set up ebp and esp
	pusha							
	mov ecx, dword [ebp+8]			;Get argument (pointer to integer)

	push ecx						;push the argument into the stack, for check function
	call check						;call check function
	add esp , 4

	cmp eax , 0						;the answer from check function is return to eax register
	je not_equal					;jump to out of range number procedure
	jmp equal
	
	not_equal:
		push	LC0					;insert pointer to "out of range" string into stack.
		call printf					;call print function
		add 	esp, 4				;Clean up stack after call
		jmp label_end

	equal:							;compute the answer to user if the number is check
		mov eax , 2
		mov ebx , 2
		cmp ecx , 1					;check if the x is 1
		mov esi , ecx				;put the x number into esi (using in diversion)
		je print
		dec ecx
		

	loop_on:						;compute 2^x ---- can do it fasten by using shifting bits
		mul ebx
		loop loop_on, ecx			;every loop ecx is dec by 1 until it equal to 0

	print:							
		mov edx , 0
		div esi						;diversion 2^x by x

		push eax					;push the answer
		push LC1					
		call printf
		add esp, 8					; Clean up stack after call

	label_end:						; Function exit code
		popa
		mov	esp, ebp				
		pop	ebp
		ret

