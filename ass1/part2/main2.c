#include <stdio.h>

extern int calc_func(int x);			/*declare there is an external function*/

int check(int x){						/*check if x is bigger then 0 and smaller then 32*/
	if (x>0 && x<32) return 1;
	return 0;
}

int main(void)
{
	
  int number;
  printf("Enter a number: ");
  fflush (stdout);

  scanf("%d",&number); 

  calc_func(number);

  return 0;
}
