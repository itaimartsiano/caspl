#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {

	int iarray[3];
	char carray[3];
	
 	printf("- iarray: %p\n",iarray);
	printf("- iarray+1: %p\n",iarray+1);
   	printf("- carray: %p\n",carray);
   	printf("- carray+1: %p\n",carray+1);
	
	return 0;
	
	/*the operator + is increase/enhance the pointer to the next cell, given the sizeof 		type	*/

}
