#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char censor(char c) {
  if(c == '!') {
    return '.';
  }
  return c;
}


void for_each(char *array, char (*f) (char)) {

	char * pointer = array;
	while (f(*pointer) != 0){
		*pointer = f(*pointer);
		pointer = pointer + 1;
	}
	*pointer =0;		
	
}

 
 
int main(int argc, char **argv){
 	
	char c[] = {'H','E','Y','!', 0};
	for_each(c, censor);
	printf("%s\n", c);

	return 0;
}


