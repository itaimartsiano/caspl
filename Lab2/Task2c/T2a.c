#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char censor(char c) {
  if(c == '!') {
    return '.';
  }
  return c;
}




void for_each(char *array, char (*f) (char)) {

	char * pointer = array;
	while ((*pointer = f(*pointer)) != 0){
		pointer = pointer+1;
	}

			
}


 /* Gets a character c, and returns it in lower case. 
 Characters that do not have a lower case, are returned unchanged */
char to_lower(char c){

	if (c>64 && c<91) return c+32;
	return c;
}


/* Prints the value of c, followed by a new line, and returns c unchanged */
char cprt(char c){
	printf("%c\n", c);
	return c;
}


/* Ignores c, reads and returns a character from stdin using fgetc. 
Returns 0 when a new line character is read. */

char my_get(char c){
	
	int in;	
	in = fgetc(stdin);
	if (in == '\n') in = 0;
	return in;

}


/* Gets a character c, ignores it, and ends the program */
char quit(char c){
	exit(0);
}



int main(int argc, char **argv){

	char c[] = {'H','E','Y','!', 0};
	for_each(c, censor);
	printf("%s\n", c);
	
	for_each(c, quit);

	for_each(c, my_get);
	for_each(c, cprt);
	for_each(c, to_lower);
	for_each(c, censor);
	for_each(c, cprt);

	return 0;
}


















