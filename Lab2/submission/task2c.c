#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char censor(char c) {
  if(c == '!') {
    return '.';
  }
  return c;
}

void for_each(char *array, char (*f) (char)) {

	char * pointer = array;
	
	do{
	    *pointer = f(*pointer);
	}while(*(pointer++) != 0);
}

char to_lower(char c){
	if (c>64 && c<91) return c+32;
	return c;
}

char cprt(char c){
	printf("%c\n", c);
	return c;
}

char my_get(char c){
	int in;	
	in = fgetc(stdin);
	if (in == '\n') in = 0;
	return in;
}

char quit(char c){
  exit(0);
}

int main(int argc, char **argv){

	char c[] = {'H','E','Y','!', 0};
	for_each(c, censor);
	printf("%s\n", c);
	
	for_each(c, my_get);
	for_each(c, cprt);
	for_each(c, to_lower);
	for_each(c, censor);
	for_each(c, cprt);

	return 0;
}


















