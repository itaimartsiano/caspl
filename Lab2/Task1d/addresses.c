﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>



int main (int argc, char** argv){
   
  int iarray[] = {1,2,3};
  char carray[] = {'a','b','c'};
  int* iarrayPtr = &iarray;
  char* carrayPtr = &carray;
  int * p;
  int i=0;
  
  for (i=0; i<3 ; i++){
    printf("iarray[%d]: %d\n",i,*iarrayPtr+i);
  }
 
  for (i=0; i<3 ; i++){
    printf("cararray[%d]: %c\n",i,*carrayPtr+i);
  }
  
  printf("- p: %p\n",p);
  
  return 0;
}
