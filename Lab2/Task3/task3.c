#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char censor(char c) {
  if(c == '!') {
    return '.';
  }
  return c;
}

void for_each(char *array, char (*f) (char)) {

	char * pointer = array;
	
	while((*(pointer)=f(*pointer)) != 0){		/*to check if if is ok to do f first*/
	    pointer ++;
	}
	
}


char to_lower(char c){
	if (c>64 && c<91) return c+32;
	return c;
}



char cprt(char c){
	printf("%c\n", c);
	return c;
}



char my_get(char c){
	
	int in;	
	in = fgetc(stdin);
	if (in == '\n') in = 0;
	
	return in;
}



char quit(char c){
  exit(0);
}


struct fun_desc {
  char *name;
  char (*fun)(char);
};




int main(int argc, char **argv){

	
	char carray [100] = "";

	struct fun_desc menu[] = { { "To Lower Case", to_lower }, { "Censor", censor }, 
				   { "Print", cprt }, { "Get string", my_get }, { "Quit", quit},
				   { NULL, NULL } };
	

	while (1){
		
		int i=0;
		int num=0;
		
		 while (menu[i].name != 0){
		  	printf("%d) %s \n", i, menu[i].name);
		 	i++;
	     }
		 printf ("Option: ");

		 num = fgetc(stdin);
		 num = num - 48;

		
		 fgetc(stdin);
		 
		 if (num<i && num >=0){
		    for_each(carray, menu[num].fun);
		    printf("DONE.\n");
		 }
		  printf("\n\n");


	}
	
	return 0;
}


















