#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {

	int c=0, i=1, j=0;
	int d=0, s=0;
	int delimiter;
	int numOfFiles=0;
	int z;
	int curr = 0;
	char file [11];
	
	FILE * currFile = stdout;
	FILE * inputfile = stdin;
	FILE * outputfiles[5];
	
	for(z=1; z<argc; z++){
	  
	  if(strcmp(argv[z],"-s")==0){
	   s = 1;
	   delimiter = *argv[++z];
	  }
	  else
	    if(strcmp(argv[z],"-d")==0){
	      d=1;
	    }
	    else 
	      if(strcmp(argv[z],"-i")==0){
		inputfile = fopen(argv[++z],"r");
	      }
	      else
		if(strcmp(argv[z],"-o")==0){
		  numOfFiles = atoi(argv[++z]);
		}
	}
	
	if (numOfFiles > 0){
	  for(j=0; j<(numOfFiles);j++){
	    printf("Enter output file %d:\n", j+1);
	    gets(file);
	    outputfiles[j] = fopen(file,"w+");
	  }
	}
	
	z=0;
	while((c=fgetc(inputfile))!=EOF){
	  
	  
	  if (i==1) {
	    
	    if (numOfFiles>0){
	      currFile =outputfiles[curr];
	      curr++;
	      curr = curr % numOfFiles;
	    }
	    
	    fprintf(currFile, "1:");
	    i++;
	  }
	 
	  if ( ((d==1) && (c>='0') && (c<='9')) || ((s==1) && (c==delimiter)) ) {
		
		fprintf(currFile,"%c", c);
		fprintf(currFile,"\n");
		
		if (numOfFiles>0){
		  currFile =outputfiles[curr];
		  curr++;
		  curr = curr % numOfFiles;
		}
		
		
		fprintf(currFile,"%d:", i);
		i++;
	  }
	  
	  else {
	   
	    switch (c){
	      case '@':
	      case '*':
		fprintf(currFile,"%c", c);   
		fprintf(currFile,"\n");
		
		if (numOfFiles>0){
		  currFile =outputfiles[curr];
		  curr++;
		  curr = curr % numOfFiles;
		}
		
		fprintf(currFile,"%d:",i);
		i++;
		break;
	      
	      default:
		fprintf(currFile,"%c", c);
	  }
	    
	  } 
	}
	fclose(inputfile);
	return 0;
}