#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  int i, c=0, lineNumber=1;
  char delimiter;
  int isS=0;
  int isD=0;
  FILE * input=stdin;
    
  for(i=1; i<argc; i++){
    if(strcmp(argv[i],"-d")==0)
    {
	/* -d is active */
	isD = 1;
    }
    else if(strcmp(argv[i],"-s")==0)
    {
	/* -s is active */
	isS = 1;
	delimiter=*argv[i+1];
    }
    else if(strcmp(argv[i],"-i")==0)
    {
	/* -i is active */
	input=fopen(argv[++i],"r");
    }
  }
    
  fflush(stdout);  
  while((c=fgetc(input))!=EOF)  
  {
      
    if (lineNumber==1)
    {
      printf("1:");
      lineNumber++;
    }
    
    if ( ((isD==1) && (c>='0') && (c<='9')) || ((isS==1) && (c==delimiter)) )
    {
      printf("%c", c);
      printf("\n" );
      printf("%d:", lineNumber);
      lineNumber++;
    }
    
    else
    {
    switch(c)
      {
      case '@' :
      case '*' :
	printf("%c", c);
	printf("\n" );
	printf("%d:", lineNumber);
	lineNumber++;
	break;
      default :
	printf("%c", c);
      }
    }
  }   
  
  printf("\n");

  return 0;
}