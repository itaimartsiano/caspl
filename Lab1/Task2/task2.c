#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  int i=0,j=0,c=0,k=0,lineNumber=1;
  char delimiter;
  int isS=0;
  int isD=0;
  int isO=0;
  int curFile=0;
  int numberOfFiles=0;
  char filename[10];
  FILE * input=stdin;
  FILE * curOutput=stdout;
  FILE * output[5];
    
  for(i=1; i<argc; i++){
    if(strcmp(argv[i],"-d")==0)
    {
	/* -d is active */
	isD = 1;
    }
    else if(strcmp(argv[i],"-s")==0)
    {
	/* -s is active */
	isS = 1;
	delimiter=*argv[i+1];
    }
    else if(strcmp(argv[i],"-i")==0)
    {
	/* -i is active */
	input=fopen(argv[++i],"r");
    }
    else if(strcmp(argv[i],"-o")==0)
    {
	/* -o is active */
	isO = 1;
	numberOfFiles = atoi(argv[i+1]);
	
	for(j=0; j<(numberOfFiles);j++){
	  printf("Enter output file %d:\n", (j+1));
	  gets(filename);
	  output[j]=fopen(filename,"w+");
	}
    }
  }
    
  fflush(stdout);  
  while((c=fgetc(input))!=EOF)  
  {  
    if (lineNumber==1)
    {
      if (isO==1) {
		  curOutput = output[curFile];
		  curFile++;
		  curFile = curFile % numberOfFiles;
		  }
	  
	  fprintf(curOutput,"1:");
	  lineNumber++;
    }
    
    if ( ((isD==1) && (c>='0') && (c<='9')) || ((isS==1) && (c==delimiter)) )
    {
		fprintf(curOutput,"%c", c);
		fprintf(curOutput,"\n");
		
		
		if (isO==1)
		{
		  curOutput = output[curFile];
		  curFile++;
		  curFile = curFile % numberOfFiles;
		}
		
		fprintf(curOutput,"%d:", lineNumber);
		lineNumber++;

    }
    
    else
    {
    switch(c)
      {
      case '@' :
      case '*' :
		fprintf(curOutput,"%c", c);
		fprintf(curOutput,"\n");
		
		
		if (isO==1)
		{
		curOutput = output[curFile];
		curFile++;
		curFile = curFile % numberOfFiles;
		}
		
		fprintf(curOutput,"%d:", lineNumber);

		lineNumber++;
	break;
      default :
	fprintf(curOutput,"%c", c);
      }
    }
  }   
  
  fprintf(curOutput,"\n");
  
  /*
  
  for(k=0; k<(numberOfFiles);k++){
	printf("\n%d\n",numberOfFiles);
	fclose(output[k]); 
	printf("\n%d\n",k);
	}
	
	 fclose(input);
	*/
  return 0;
}