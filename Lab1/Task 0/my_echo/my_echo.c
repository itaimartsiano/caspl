#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {

	int c=0, i=0;
	char output[300];
	
	printf("write your text:\n");
	  

	/*one way to get input - more complex*/	
	while((c=fgetc(stdin))!='\n'){
		output[i++]=(char)c;		/*first it insert the char into i and only after it increase the "i"*/
	}

	output[i]=0;	
	puts(output);   



	printf("write your text:\n");

	/*second way to get input from user more easy - there is more options with scanf and others*/
	if( fgets (output, 300, stdin)!=NULL ) {
     
     		puts(output);
	}

	return 0;
}
