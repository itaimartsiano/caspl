section .data                    	; data section, read-write
        an:    DD 0              	; this is a temporary var

section .text                    	; our code is always in the .text section
        global strToLeet          	; makes the function appear in global scope
        extern printf            	; tell linker that printf is defined elsewhere 				; (not used in the program)

strToLeet:                        	; functions are defined as labels
        push    ebp              	; save Base Pointer (bp) original value
        mov     ebp, esp         	; use base pointer to access stack contents
        pushad                   	; push all variables onto stack
        mov ecx, dword [ebp+8]	    ; get function argument

;;;;;;;;;;;;;;;; FUNCTION EFFECTIVE CODE STARTS HERE ;;;;;;;;;;;;;;;; 

	mov	dword [an], 0		; initialize answer
label_here:

    cmp byte [ecx], 65
    je is_A

    cmp byte [ecx], 66
    je is_B

    cmp byte [ecx], 67
    je is_C

    cmp byte [ecx], 68
    je is_Cap

    cmp byte [ecx], 69
    je is_E

    cmp byte [ecx], 70
    je is_Cap

    cmp byte [ecx], 71
    je is_G

    cmp byte [ecx], 72
    je is_H

    cmp byte [ecx], 73
    je is_I

    cmp byte [ecx], 74
    je is_Cap

    cmp byte [ecx], 75
    je is_Cap

    cmp byte [ecx], 76
    je is_L

    cmp byte [ecx], 77
    je is_Cap

    cmp byte [ecx], 78
    je is_Cap

    cmp byte [ecx], 79
    je is_O

    cmp byte [ecx], 80
    je is_Cap

    cmp byte [ecx], 81
    je is_Cap

    cmp byte [ecx], 82
    je is_Cap

    cmp byte [ecx], 83
    je is_S

    cmp byte [ecx], 84
    je is_T

    cmp byte [ecx], 85
    je is_Cap

    cmp byte [ecx], 86
    je is_Cap

    cmp byte [ecx], 87
    je is_Cap

    cmp byte [ecx], 88
    je is_Cap

    cmp byte [ecx], 89
    je is_Cap

    cmp byte [ecx], 90
    je is_Z


jmp label_end    


is_A:
    mov byte [ecx], 52 
    inc dword [an]
    jmp label_end

is_B:
    mov byte [ecx], 56
    inc dword [an]
    jmp label_end

is_C:
    mov byte [ecx], 40
    inc dword [an]
    jmp label_end

is_E:
    mov byte [ecx], 51 
    inc dword [an]
    jmp label_end

is_G:
    mov byte [ecx], 54
    inc dword [an]
    jmp label_end

is_H:
    mov byte [ecx], 35
    inc dword [an]
    jmp label_end

is_I:
    mov byte [ecx], 33 
    inc dword [an]
    jmp label_end

is_L:
    mov byte [ecx], 49
    inc dword [an]
    jmp label_end

is_O:
    mov byte [ecx], 48
    inc dword [an]
    jmp label_end

is_S:
    mov byte [ecx], 53
    inc dword [an]
    jmp label_end

is_T:
    mov byte [ecx], 55 
    inc dword [an]
    jmp label_end

is_Z:
    mov byte [ecx], 50
    inc dword [an]
    jmp label_end

is_Cap:
    add byte [ecx], 32
    jmp label_end


label_end:
    inc	ecx      		; increment pointer
	cmp	byte [ecx], 0    		; check if byte pointed to is zero
	jnz	label_here       		; keep looping until it is null terminated

;;;;;;;;;;;;;;;; FUNCTION EFFECTIVE CODE ENDS HERE ;;;;;;;;;;;;;;;; 
         popad                    ; restore all previously used registers
         mov     eax,[an]         ; return an (returned values are in eax)
         mov     esp, ebp
         pop     ebp
         ret
