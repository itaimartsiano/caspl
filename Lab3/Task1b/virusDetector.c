#include <stdio.h>
#include <stdlib.h>

void PrintHex(char * buffer, int array_size)
{
	int i;

 	for (i=0 ; i<array_size ; i++){
 		printf("%02hhX ",buffer[i]);
 	}
}
  
  
typedef struct virus virus;
 
struct virus {
        int length;
        char *signature;
        char *name;
        virus *next;
};


 /* Print the data of every link in list. Each item followed by a newline character. */
void list_print(virus *virus_list){
  
  virus * currVirus = virus_list;
  
  while (currVirus != NULL){
    
    printf("virus name: %s\n", currVirus->name);
    printf("virus size: %d\n", currVirus->length);
    printf("signature: \n");
    PrintHex(currVirus->signature , currVirus->length);
    printf("\n");
    printf("\n");
    currVirus = currVirus->next;
    
  }
  
}
    
     /* Add a new link with the given data to the list 
        (either at the end or the beginning, whichever you think is more convenient),
        and return a pointer to the list (i.e., the first link in the list).
        If the list is null - create a new entry and return a pointer to the entry. */
 
virus * list_append(virus *virus_list, virus *data){
  
  if (virus_list == NULL){
   data->next = NULL;
   return data;
  }
  
  virus * currVirus = virus_list;
  
  while (currVirus->next != NULL){
    currVirus = currVirus->next;
  }

  data->next = NULL;
  currVirus->next = data;
  return virus_list;
  
}
 /* Free the memory allocated by the list. */
void list_free(virus *virus_list){

  virus * prev = virus_list;
  virus * curr = virus_list->next;

  while(curr != NULL){
    free (prev->signature);
    free (prev->name);
    free (prev);    
    prev = curr;
    curr = curr->next;
  }
  
  free (prev->signature);
  free (prev->name);
  free (prev);

}
  



int main(int argc, char **argv) {
 
  FILE * input = fopen("signatures","r");
  virus * virus_list = NULL;
  
  if (input == NULL){
    return(-1);
  }
  
  int lengthOfVirus;
  while (fread(&lengthOfVirus , 4,1, input) != 0){
    
    char * virusSignature = (char *) malloc(lengthOfVirus);
    int i = 0;;
    
    fread(virusSignature, lengthOfVirus,1, input);  
    
    char * virusName = (char *) malloc(101);
    
    while (i<100){
      fread(virusName+i , 1,1, input);
      if (virusName[i] == 0) break;
      i++;
    }
    
    virus * newVirus = (virus *) malloc(sizeof(virus));
    newVirus->length = lengthOfVirus;
    newVirus->name = virusName;
    newVirus->signature = virusSignature;
    virus_list = list_append (virus_list, newVirus);
    
  }

  list_print(virus_list);
  list_free(virus_list);

  
  fclose(input);
  return 0;
}
