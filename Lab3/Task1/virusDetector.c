#include <stdio.h>
#include <stdlib.h>

void PrintHex(char * buffer, int array_size)
{
	int i;

 	for (i=0 ; i<array_size ; i++){
 		printf("%02hhX ",buffer[i]);
 	}
}

      
int main(int argc, char **argv) {
  FILE * input = fopen("signatures","r");
  
  if (input == NULL){
    return(-1);
  }
  
  int lengthOfVirus;
  while (fread(&lengthOfVirus , 4,1, input) != 0){
    
    char virusSignature[lengthOfVirus];
    int i;
    

    fread(virusSignature , lengthOfVirus, 1, input);  
   
    
    
    i=0;
    char virusName[101];
    
    while (i<100){
      fread(virusName+i , 1,1, input);
      if (virusName[i] == 0) break;
      i++;
    }
    
    printf("virus name: %s\n", virusName);
    printf("virus size: %d\n", lengthOfVirus);
    printf("signature: \n");
    PrintHex(virusSignature , lengthOfVirus);
    printf("\n");
    printf("\n");
  }
  
  fclose(input);
  return 0;
}
