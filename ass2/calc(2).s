STKSIZE EQU 20
LINE_LENGTH EQU 81
LINK_SIZE EQU 5

section	.rodata
	LC0: DB	"number of opernads had done is:", 10, 0	; Format answer
	P_INT: DB "%02X",0
	INT: DB "%d\n",0
	STRING_PRINT: DB "%c\n",10,0


section .bss
	

section .data
	operands_stack times STKSIZE db 0			;opernds stack
	tmp_pointer: dd 0
	tmp_pointer_to_push_back: dd 0
	tmp_pointer_to_release: dd 0
	Line_From_User:	times LINE_LENGTH db 0		;buffer for user input
	stack_counter: db 0							;stack size
	one_bit_counter: db 0							;one bit counter
	



section .text
	align 16
	extern gets
	extern malloc
	extern printf
	extern free
	global main
	

	%macro print_string 1
		jmp %%bla
		%%str: DB %1 , 10,0
		%%bla:
		pushf
		pusha
		push %%str
		call printf
		add esp,4
		popa
		popf
	%endmacro


	%macro startFunc 0
		push	ebp
		mov	ebp, esp	; entry code
		pushad		; Save registers
	%endmacro
	

	%macro endFunc 0
		popad			; Restore registers
		mov	esp, ebp	; Function exit code
		pop	ebp
		ret
	%endmacro	

	%macro insert_new_link 0
		pushad
		push dword LINK_SIZE
		call malloc					;get memory for new link
		mov edx, dword [tmp_pointer]
		mov dword [eax+1], edx 
		mov	[tmp_pointer], eax   	; insert new link into linked list
		add esp, 4
		popad
	%endmacro



main:

	startFunc
	call my_calc
	;read ans from eax
	;call printText
	
	endFunc
	







; my calc (function)---------------------------------------------------------

my_calc:

	startFunc

while_not_quit:

	push Line_From_User
	call gets	;return pointer to string
	add esp, 4

	cmp byte [eax], 0
	je while_not_quit

	cmp byte [eax], 'q'
	je quit_loop

	cmp byte [eax], 'd'
	je label_duplicate

	cmp byte [eax], 'p'
	je label_pop_and_print

	cmp byte [eax], 'n'
	je label_one_bit

	cmp byte [eax], '+'
	je label_addition

	push Line_From_User
	call handle_number_from_user 			;convert the number to hex
	add esp, 4

	push eax
	call func_insert_stack
	add esp,4


	jmp while_not_quit
	
label_duplicate:
	call func_duplicate
	jmp while_not_quit

label_pop_and_print:
	call func_pop_and_print
	jmp while_not_quit

label_one_bit:
	call func_one_bit
	jmp while_not_quit	

label_addition:
	call func_addition
	jmp while_not_quit	

quit_loop:
	call func_quit
	endFunc


	endFunc












;-----------------------------------------------------------------------------------

func_addition:
	startFunc

	call pop_operand
	mov ebx, eax
	cmp ebx, 0
	je push_back_ebx
	call pop_operand
	mov ecx, eax
	cmp ecx, 0
	je push_back_ecx						;we check that there is enough operands

	push ebx
	push ecx
	call check_shorter
	add esp, 8								
	cmp eax, 0		
	je replace_ebx_ecx						;0 - ebx is shorter, 1 - ecx is shorter

	mov [tmp_pointer_to_push_back], ebx 
	mov [tmp_pointer_to_release], ecx

ebx_is longer:
	xor eax, eax
	shr eax, 1								;to be sure 0 is in CF

	mov dl, [ecx]
	add byte [ebx], dl
	jc carry 								;if there is carry after addition


carry:



replace_ebx_ecx:




push_back_ecx:
	push ecx
	call func_insert_stack
	add esp, 4
	endFunc

push_back_ebx:
	push ebx
	call func_insert_stack
	add esp, 4
	endFunc





	;save to memory to release ecx and ebx
	;add ebx ecx
	;jc carry
	;else no carry

	;carry
	;insert new link
	;mov the new data in the relevamt byte
	;update ebx and ecx to nex




;-----------------------------------------------------------------------------------

func_one_bit:

	startFunc
	call pop_operand				;import the last operand in the operands_stack
	cmp eax, 0
	je end_func_one_bit				;if there is no operands in Stack

	mov [tmp_pointer_to_release], eax			;save the num to release it in the end
	mov dword [tmp_pointer], 0
	mov byte [one_bit_counter], 0	;reset bit counter
	
move_to_next_link:
	cmp eax, 0
	je insert_last_data
	mov ecx, 8
	mov bl, byte [eax]				;mov the data of this link into bl

shift_bits_right:
	shr ebx, 1
	jc count_up
	
	continue_shifting1:
	mov dl, byte [one_bit_counter]
	cmp dl, 255
	je insert_to_new_link
	continue_shifting:
	loop shift_bits_right, ecx 		;do the loop for 8 times

	mov eax, [eax+1] 				;enhance to next pointer
	jmp move_to_next_link

count_up:
	inc byte [one_bit_counter]
	jmp continue_shifting1


insert_to_new_link:
	insert_new_link
	mov edx ,[tmp_pointer]
	mov byte [edx], 255
	mov byte [one_bit_counter], 0	;reset bit counter
	jmp continue_shifting


insert_last_data:
	mov dl, [one_bit_counter]
	cmp dl, 0
	je edx_0
	insert_new_link
	mov ebx ,[tmp_pointer]
	mov byte [ebx], dl
	
	edx_0:
	mov ebx, [tmp_pointer]
	
	push ebx
	call func_insert_stack
	add esp, 4
	
	mov ebx, [tmp_pointer_to_release]
	push ebx
	call release_operand
	add esp, 4

end_func_one_bit:
	endFunc
















;-----------------------------------------------------------------------------------

func_duplicate:
	
	startFunc
	call pop_operand			;try to pop operand
	cmp eax, 0
	je end_func_dup

	push eax					;push back to stack the last element
	call func_insert_stack
	add esp, 4

	mov dword [tmp_pointer], 0
	push eax
	call func_duplicate_recursive
	add esp, 4

	push eax
	call func_insert_stack
	add esp, 4
	
end_func_dup:
	endFunc




;-----------------------------------------------------------------------------------

func_duplicate_recursive:
	
	startFunc
	mov ecx, [ebp+8]

	xor ebx, ebx

	cmp ecx, 0
	je end_recursive_dup_call
	mov byte bl, [ecx]			;mov the data byte to bl
	mov dword ecx, [ecx+1]		;enhance the pointer to next link

	pushad
	push ecx
	call func_duplicate_recursive		;recursive calling to this function
	add esp, 4
	popad

	pushad
	push dword LINK_SIZE
	call malloc					;get memory for new link
	mov edx, dword [tmp_pointer]
	mov dword [eax+1], edx 
	mov	[tmp_pointer], eax   	; insert new link into linked list
	add esp, 4
	popad

	mov eax, [tmp_pointer]
	mov [eax], bl
	
end_recursive_dup_call:
	popad						;Restore registers
	mov eax, [tmp_pointer]
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret




;-----------------------------------------------------------------------------------

func_pop_and_print:
	
	startFunc
	call pop_operand
	cmp eax, 0
	je end_pop_and_print


	push eax
	call func_print_number
	add esp, 4

	push eax
	call release_operand
	add esp, 4

end_pop_and_print:
	endFunc






;-----------------------------------------------------------------------------------

release_operand:
	
	startFunc
	mov ecx, dword [ebp+8]		;get first argument to release
	mov ebx, dword [ebp+8]

	cmp ecx, 0
	je end_release_number

	mov dword ecx, [ecx+1]		;enhance the pointer to next link

	pushad
	push ecx
	call release_operand		;recursive calling to this function
	add esp, 4
	popad

	push ebx
	call free
	add esp, 4

	end_release_number:
	endFunc










;-----------------------------------------------------------------------------------

pop_operand:

  startFunc
  xor ebx, ebx

  mov bl, [stack_counter]				;check if stack is empty
  cmp bl, 0
  je stack_is_empty

  dec bl
  mov eax, dword [operands_stack + 4*ebx]					;insert the operand into the top of stack

  mov byte [stack_counter], bl

  mov [tmp_pointer], eax
  popad									;Restore registers
  mov eax, [tmp_pointer]
  mov	esp, ebp						;Function exit code
  pop	ebp	
  ret


stack_is_empty:

  print_string "Stack is empty"
  popad									;Restore registers
  xor eax, eax
  mov	esp, ebp						;Function exit code
  pop	ebp	
  ret







;-----------------------------------------------------------------------------------

func_quit:
	
	startFunc

more_operands_in_stack:	
	
	mov cl, [stack_counter]
	cmp cl, 0
	je ret_to_quit

	call pop_operand
	push eax
	call release_operand
	add esp, 4

	jmp more_operands_in_stack

ret_to_quit:
	endFunc









;-----------------------------------------------------------------------------------
func_insert_stack:
  
  startFunc
  mov ecx, dword [ebp+8]
  xor ebx, ebx

  mov bl, [stack_counter]				;check if stack is full
  cmp bl, 4
  ja stack_is_full

  mov dword [operands_stack + ebx*4], ecx					;insert the operand into the top of stack

  inc byte [stack_counter]

  endFunc


stack_is_full:

  print_string "Stack is full"

  push ecx
  call release_operand
  add esp, 4
  
  endFunc















;----------------------------------------------------------------------------------------

func_print_number:
	

	startFunc
	mov ecx, dword [ebp+8]		;get first argument to print
	xor ebx, ebx

	cmp ecx, 0
	je end_print_number
	mov byte bl, [ecx]
	mov dword ecx, [ecx+1]		;enhance the pointer to next link

	pushad
	push ecx
	call func_print_number		;recursive calling to this function
	add esp, 4
	popad

	push ebx
	push P_INT
	call printf
	add esp, 8
	
	end_print_number:
	endFunc

	;TODO: fix not to print zero 
	;TODO: new line in end of num











;--------------------------------------------------------------------------------------


handle_number_from_user:
	
	startFunc
    mov ecx, dword [ebp+8]		;get user string number
	mov dword [tmp_pointer], 0	;initial tmp pointer to zero
	xor esi, esi				;reset counter of data in link
	
	push ecx
	call check_length_is_odd
	add esp, 4

	cmp eax, 0
	je while_not_end_of_line	;if the length number is even jump to while

	pushad
	push dword LINK_SIZE
	call malloc					;get memory for the first link
	mov dword [eax+1], 0 
	mov	[tmp_pointer], eax   	; insert new link into linked list
	add esp, 4
	popad
	xor ebx, ebx
	inc esi
	

while_not_end_of_line:

	cmp byte [ecx], 0			;if it is the end of line
	je end_of_line

	cmp si, 0					;if there is already exist link which is not full
	ja no_new_link

	pushad
	push dword LINK_SIZE
	call malloc					;get memory for new link
	mov edx, dword [tmp_pointer]
	mov dword [eax+1], edx 
	mov	[tmp_pointer], eax   	; insert new link into linked list
	add esp, 4
	popad
	
	xor ebx, ebx

no_new_link:
	
	cmp byte [ecx], 58			;if it is number between 0 to 9
	jb dec_number

	shl bl, 4					;if it number between A to F
	mov bh, byte [ecx]
	sub bh, 55
	xor bl, bh
	xor bh, bh

	cmp esi, 1
	je reset_counter			;if the number of numbers in this link is 2 then go to new link

	inc esi
	inc ecx

	jmp while_not_end_of_line


dec_number:
	
	shl bl, 4
	mov bh, byte [ecx]
	sub bh, 48
	xor bl, bh
	xor bh, bh
	
	cmp esi, 1
	je reset_counter

	add esi, 1
	inc ecx

	jmp while_not_end_of_line


end_of_line:

	mov eax, [tmp_pointer]
	mov [eax], bl 				;insert the hex num into the data field in link
	jmp ret_label


reset_counter:
	
	xor esi, esi
	mov eax, [tmp_pointer]
	mov [eax], bl 				;insert the hex num into the data field in link
	inc ecx
	jmp while_not_end_of_line


ret_label:
	popad						;Restore registers
	mov eax, [tmp_pointer]
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret




;------------------------------------------------------------------------------------

check_length_is_odd:

	startFunc
	xor eax,eax
	mov ecx, dword [ebp+8]

real_number:
	cmp byte [ecx], 0
	jne add_one
	jmp check_odd

add_one:
	inc eax
	inc ecx
	jmp real_number

check_odd:
	mov ebx, 2
	div bl
	cmp ah, 0
	je ret_even

	popad
	mov eax, 1
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret

ret_even:
	popad
	mov eax, 0
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret	








;--------------------------------------------------------------------------------------

check_shorter:

	startFunc
	mov ebx, [ebp+8]
	mov ecx, [ebp+12]

start_again:
	cmp ebx, 0
	je ebx_is_shorter
	cmp ecx, 0
	je ecx_is_shorter
	mov ebx, [ebx+1]
	mov ecx, [ecx+1]
	jmp start_again

ebx_is_shorter:
	popad
	mov eax, 1					;return 1 if the second push argument is shorter
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret	


ecx_is_shorter:
	popad
	mov eax, 0					;return 0 if the first push argument is shorter
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret






; x*2^Y (function)-------------------------------

; x*2^-Y (function)-------------------------------







