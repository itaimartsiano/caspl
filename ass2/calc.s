
STKSIZE EQU 20
LINE_LENGTH EQU 81
LINK_SIZE EQU 5



%macro print_string 1					;macro to print strrings to screen
	jmp %%bla
	%%str: DB %1 , 10,0
	%%bla:
	pushf
	pusha
	push %%str
	call printf
	add esp,4
	popa
	popf
%endmacro


%macro startFunc 0						;macro collect all the start function commands
	push	ebp
	mov	ebp, esp	; entry code
	pushad		; Save registers
%endmacro


%macro endFunc 0						;macro collect all the end void function commands
	popad								;Restore registers
	mov	esp, ebp						;Function exit code
	pop	ebp
	ret
%endmacro	


%macro insert_new_link 0				;macro to insert new link to link list
	pushad
	push dword LINK_SIZE
	call malloc							;get memory for new link
	mov edx, dword [tmp_pointer]
	mov dword [eax+1], edx 
	mov	[tmp_pointer], eax   			;insert new link into linked list
	add esp, 4
	popad
%endmacro



section	.rodata
	END_SENTENCE: DB	"number of operations had done is: %d", 10, 0	; Format answer
	P_INT: DB "%02X", 0
	P_INT_0: DB "%01X", 0
	INT: DB "%d\n",0
	STRING_PRINT: DB "%c\n",10,0


section .bss
	

section .data
	operands_stack times STKSIZE db 0					;opernds stack
	tmp_pointer: dd 0
	tmp_pointer_to_push_back: dd 0
	tmp_pointer_to_release: dd 0
	Line_From_User:	times LINE_LENGTH db 0				;buffer for user input
	stack_counter: db 0									;stack size
	links_counter: db 0									;links counter
	operations_counter: dd 0							;count num of operations
	


section .text
	align 16
	global main
	extern gets
	extern malloc
	extern printf
	extern free
	

main:

	startFunc
	call my_calc
	
	push eax
	push END_SENTENCE
	call printf								;print num of operations
	add esp, 8
	
	endFunc
	




; my calc --------------------------------------------------------------------------------------------------

my_calc:

	startFunc

while_not_quit:

	push Line_From_User
	call gets								;return pointer to string
	add esp, 4

	cmp byte [eax], 0
	je while_not_quit

	cmp byte [eax], 'q'
	je quit_loop

	cmp byte [eax], 'd'
	je label_duplicate

	cmp byte [eax], 'p'
	je label_pop_and_print

	cmp byte [eax], 'n'
	je label_one_bit

	cmp byte [eax], '+'
	je label_addition

	cmp byte [eax], '^'
	je label_2_mul

	cmp byte [eax], 'v'
	je label_2_div

	push eax
	call func_check_input					;check if the input is legal
	add esp, 4

	cmp eax, 0
	je while_not_quit

	push eax
	call handle_number_from_user 			;convert the number to hex
	add esp, 4

	push eax
	call func_insert_stack
	add esp,4


	jmp while_not_quit
	
label_2_div:
	call func_2_div
	inc dword [operations_counter]
	jmp while_not_quit

label_2_mul:
	call func_2_mul
	inc dword [operations_counter]
	jmp while_not_quit


label_duplicate:
	call func_duplicate
	inc dword [operations_counter]
	jmp while_not_quit

label_pop_and_print:
	call func_pop_and_print
	inc dword [operations_counter]
	jmp while_not_quit

label_one_bit:
	call func_one_bit
	inc dword [operations_counter]
	jmp while_not_quit	

label_addition:
	call func_addition
	inc dword [operations_counter]
	jmp while_not_quit	

quit_loop:
	call func_quit
	popad								;Restore registers
	mov eax, [operations_counter]
	mov	esp, ebp						;Function exit code
	pop	ebp
	ret




;--------------------------------------------------------------------------------------------

func_check_input:
	
	startFunc
	mov ecx, [ebp+8]
	mov edx, [ebp+8]

front_check:
	cmp byte [ecx] , 0
	je end_check_input
	cmp byte [ecx] , '0'
	je zero_in_front
	jmp regular_check

zero_in_front:
	inc edx
	inc ecx
	jmp front_check

regular_check:
	cmp byte [ecx] , 0
	je valid_input
	cmp byte [ecx] , '0'
	jb invalid_char
	jae check_smaller_then_9

check_smaller_then_9:
	cmp byte [ecx] , '9'
	ja check_bigger_then_A
	inc ecx
	jmp regular_check


check_bigger_then_A:
	cmp byte [ecx] , 'A'
	jae check_smaller_then_G
	jb invalid_char

check_smaller_then_G:
	cmp byte [ecx] , 'G'
	jae invalid_char
	inc ecx
	jmp regular_check

invalid_char:
	print_string "you have enterd wrong input"
	popad									;Restore registers
	mov eax, 0
	mov	esp, ebp							;Function exit code
	pop	ebp
	ret

end_check_input:
	cmp byte [edx], 0
	jne valid_input
	dec edx

valid_input:
	
	mov dword [tmp_pointer], edx
	popad									;Restore registers
	mov eax, [tmp_pointer]
	mov	esp, ebp							;Function exit code
	pop	ebp
	ret
	

	

;-----------------------------------------------------------------------------------

func_2_div:
	
	startFunc

	push dword 1							;tell the func mul_div to div
	call func_2_mul_div
	add esp, 4

	endFunc

	

;-----------------------------------------------------------------------------------

func_2_mul:
	
	startFunc
	
	push dword 0							;tell the func mul_div to mul
	call func_2_mul_div
	add esp, 4

	endFunc



;-----------------------------------------------------------------------------------

func_2_mul_div:
	
	startFunc
	mov esi, [ebp+8]							;0 - mul, shift left, 1 - div, shift right
	call pop_operand
	cmp eax, 0
	je not_enougth_operands 					;the stack is empty

	mov ebx, eax
	mov dword [tmp_pointer_to_push_back], ebx

	call pop_operand
	cmp eax, 0
	je not_enougth_operands_eax					;not enougth opernads in Stack

	mov edx, eax
	mov dword [tmp_pointer_to_release], edx

	push edx
	call move_num_to_register					;get num of times to mul in register
	add esp ,4
	mov ecx, eax

	cmp byte [links_counter], 4					;check if the link size is under assumption
	ja error_num_is_big

	cmp ecx, 0
	je end_succesful

	cmp esi, 0
	jne div_ecx_times
	
mul_ecx_times:									;mul ecx time the ebx
	push ebx
	call func_shift_left
	add esp, 4
	loop mul_ecx_times, ecx
	jmp end_succesful

div_ecx_times:									;div ecx time the ebx
	push ebx
	call func_shift_right
	add esp, 4
	loop div_ecx_times, ecx
	jmp end_succesful
	

not_enougth_operands:
	print_string "Error: Not Enough Arguments on Stack"	
	endFunc

not_enougth_operands_eax:
	print_string "Error: Not Enough Arguments on Stack"
	push ebx
	call func_insert_stack
	add esp, 4
	endFunc

error_num_is_big:
	push edx
	call func_insert_stack
	add esp, 4

	push ebx
	call func_insert_stack
	add esp, 4
	endFunc

end_succesful:
	push ebx
	call func_insert_stack
	add esp, 4

	push edx
	call release_operand
	add esp, 4

	endFunc

	



;-----------------------------------------------------------------------------------

move_num_to_register:
	
	startFunc
	mov edx, [ebp+8]
	mov ecx, 4
	xor ebx, ebx

	push edx
	call num_of_links
	add esp, 4
	xor ecx, ecx
	mov cl, byte [links_counter]
	cmp cl, 4
	ja error_big_num
	
move_to_bl:
	shl ebx, 8
	mov bl, byte [edx]
	mov edx, [edx+1]
	loop move_to_bl, ecx

	mov cl, byte [links_counter]
	xor edx, edx

move_to_dl:
	shl edx, 8
	mov dl, bl
	shr ebx, 8
	loop move_to_dl, ecx

	mov dword [tmp_pointer], edx
	
	popad						;Restore registers
	mov eax, [tmp_pointer]
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret
	
error_big_num:
	print_string "Y number is bigger than the assumption rules"
	endFunc

	







;-----------------------------------------------------------------------------------

func_shift_left:
	startFunc
	mov edx, [ebp+8]

shifting_left:
	cmp edx, 0
	je end_shift_left

	shl byte [edx], 1 
	jc carry_left_shifting

no_carry_in_left_shift:								;there is no carry
	mov edx, [edx+1]
	jmp shifting_left


carry_left_shifting:								;there is carry
	cmp dword [edx+1], 0
	je insert_new_link_for_carry_left
	mov edx, [edx+1]
	shl byte [edx], 1
	jc carry_left_shifting
	add byte [edx], 1
	mov edx, [edx+1]
	jmp shifting_left


insert_new_link_for_carry_left:						;have to extend the link list to carry
	mov dword [tmp_pointer], 0
	insert_new_link
	mov eax, [tmp_pointer]
	mov [edx+1], eax
	mov byte [eax], 1
	jmp end_shift_left

end_shift_left:
	endFunc




;-----------------------------------------------------------------------------------

func_shift_right:
	startFunc
	mov edx, [ebp+8]
	mov dword [tmp_pointer], 0				;tmp_pointer use to save the prev link

shifting_right:
	cmp edx, 0
	je end_shift_right

	shr byte [edx], 1 
	jc carry_right_shifting

no_carry_in_right_shift:
	mov dword [tmp_pointer], edx
	mov edx, [edx+1]
	jmp shifting_right


carry_right_shifting:
	cmp dword [tmp_pointer], 0				;check if there is prev to curr
	je shift_right_the_next_link			;there is no meaning to carry
	mov eax ,[tmp_pointer]					
	add byte [eax], 128						;add 1 in the last bit, forsure there was 0 before
	cmp byte [edx+1], 0						;check if it is zero link (data)
	je check_release_link
	
	mov [tmp_pointer], edx					;update the prev to curr
	mov edx, [edx+1]						;enhance to next
	jmp shifting_right

check_release_link:
	cmp byte [edx], 0
	je release_link 
	mov [tmp_pointer], edx					;update the prev to curr
	mov edx, [edx+1]						;enhance to next
	jmp shifting_right

shift_right_the_next_link:
	mov dword [tmp_pointer], edx
	mov edx, [edx+1]
	jmp shifting_right	


release_link:
	pushad
	push edx
	call free
	add esp, 4
	popad

	mov eax ,[tmp_pointer]
	mov dword [eax+1], 0
	endFunc


end_shift_right:
	endFunc






;-----------------------------------------------------------------------------------

func_addition:
	startFunc

	call pop_operand
	mov ebx, eax
	cmp ebx, 0
	je push_back_ebx
	
	call pop_operand
	mov ecx, eax
	cmp ecx, 0
	je push_back_ecx						;we check that there is enough operands

	push ebx
	push ecx
	call check_shorter
	add esp, 8								
	cmp eax, 0		
	je replace_ebx_ecx						;0 - ebx is shorter, 1 - ecx is shorter

	mov [tmp_pointer_to_push_back], ebx 
	mov [tmp_pointer_to_release], ecx
	xor eax, eax
	shr eax, 1								;to be sure 0 is in CF

ebx_is_longer:

	jc carry 								;if there is carry after addition

no_carry:
	
	cmp dword [ebx+1], 0		
	je no_ebx_no_carry

	cmp dword [ecx+1], 0
	je no_ecx_no_carry

	mov dl, [ecx]
	add byte [ebx], dl
	mov ebx, [ebx+1]
	mov ecx, [ecx+1]
	jmp ebx_is_longer

carry:
	mov dl, [ecx]
	add byte [ebx], dl
	
	cmp dword [ebx+1], 0			
	je no_ebx_with_carry
	mov ebx, [ebx+1]
	
	cmp dword [ecx+1], 0
	je no_ecx_with_carry
	mov ecx, [ecx+1]

	mov dl, 1
	add byte [ebx], dl
	jmp ebx_is_longer


no_ecx_no_carry:
	mov dl, [ecx]
	add byte [ebx], dl
	jc no_ecx_with_carry
	jmp release_and_push

no_ebx_no_carry:
	mov dl, [ecx]
	add byte [ebx], dl
	jc no_ebx_with_carry
	jmp release_and_push
	

no_ebx_with_carry:
	
	mov dword [tmp_pointer], 0
	insert_new_link
	mov edx, [tmp_pointer]
	mov [ebx+1], edx
	mov byte [edx], 1
	jmp release_and_push


no_ecx_with_carry:
	
	cmp dword [ebx+1], 0			;check if next of ebx is not null
	je no_ebx_with_carry			;if null jump to no ebx label
	mov dl, 1
	mov ebx, [ebx+1]				;enhance the pointer to next
	add byte [ebx], dl 				
	jc no_ecx_with_carry			;if there is more carry jump to no ecx
	jmp release_and_push



replace_ebx_ecx:
	mov [tmp_pointer_to_push_back], ecx 
	mov [tmp_pointer_to_release], ebx
	mov ebx, [tmp_pointer_to_push_back]
	mov ecx, [tmp_pointer_to_release]
	xor eax, eax
	shr eax, 1								;to be sure 0 is in CF
	jmp ebx_is_longer



push_back_ecx:
	print_string "Error: Not Enough Arguments on Stack"
	push ebx
	call func_insert_stack
	add esp, 4
	endFunc

push_back_ebx:
	print_string "Error: Not Enough Arguments on Stack"
	endFunc


release_and_push:
	
	mov ebx, [tmp_pointer_to_push_back]
	mov ecx, [tmp_pointer_to_release]
	
	push ebx
	call func_insert_stack
	add esp, 4
	
	push ecx
	call release_operand
	add esp, 4
	
	endFunc










;-----------------------------------------------------------------------------------

func_one_bit:

	startFunc
	call pop_operand				;import the last operand in the operands_stack
	cmp eax, 0
	je end_func_one_bit				;if there is no operands in Stack

	mov [tmp_pointer_to_release], eax			;save the num to release it in the end
	mov dword [tmp_pointer], 0
	insert_new_link
	mov edx, [tmp_pointer]
	mov byte [edx], 0
	
move_to_next_link:
	cmp eax, 0
	je label_insert_and_release
	mov ecx, 8
	mov bl, byte [eax]				;mov the data of this link into bl

shift_bits_right:
	shr ebx, 1
	jc count_up

	continue_shifting:
	loop shift_bits_right, ecx 		;do the loop for 8 times

	mov eax, [eax+1] 				;enhance to next pointer
	jmp move_to_next_link

count_up:
	add byte [edx], 1
	jc insert_to_new_link
	jmp continue_shifting


insert_to_new_link:
	cmp dword [edx+1], 0
	jne insert_to_next_link

	insert_new_link 				;there is no next link, therefor make new one
	add byte [edx], 1				
	mov edx, [tmp_pointer]			;update edx to the new link
	
	jmp continue_shifting


insert_to_next_link:
	mov edx, [edx+1]
	add byte [edx], 1
	jc insert_to_new_link			;if the add of carry to the next link make more carry
	jmp continue_shifting


label_insert_and_release:
	
	push edx
	call func_insert_stack
	add esp, 4
	
	mov ebx, [tmp_pointer_to_release]
	push ebx
	call release_operand
	add esp, 4
	endFunc

end_func_one_bit:
	print_string "Error: Not Enough Arguments on Stack"
	endFunc






;-----------------------------------------------------------------------------------

func_duplicate:
	
	startFunc
	call pop_operand			;try to pop operand
	cmp eax, 0
	je end_func_dup

	push eax					;push back to stack the last element
	call func_insert_stack
	add esp, 4

	mov dword [tmp_pointer], 0
	push eax
	call func_duplicate_recursive
	add esp, 4

	push eax
	call func_insert_stack
	add esp, 4
	endFunc
	
end_func_dup:
	print_string "Error: Not Enough Arguments on Stack"
	endFunc




;-----------------------------------------------------------------------------------

func_duplicate_recursive:
	
	startFunc
	mov ecx, [ebp+8]

	xor ebx, ebx

	cmp ecx, 0
	je end_recursive_dup_call
	mov byte bl, [ecx]			;mov the data byte to bl
	mov dword ecx, [ecx+1]		;enhance the pointer to next link

	pushad
	push ecx
	call func_duplicate_recursive		;recursive calling to this function
	add esp, 4
	popad

	pushad
	push dword LINK_SIZE
	call malloc					;get memory for new link
	mov edx, dword [tmp_pointer]
	mov dword [eax+1], edx 
	mov	[tmp_pointer], eax   	; insert new link into linked list
	add esp, 4
	popad

	mov eax, [tmp_pointer]
	mov [eax], bl
	
end_recursive_dup_call:
	popad						;Restore registers
	mov eax, [tmp_pointer]
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret




;-----------------------------------------------------------------------------------

func_pop_and_print:
	
	startFunc
	call pop_operand
	cmp eax, 0
	je end_pop_and_print


	push eax
	call func_print_number
	add esp, 4
	print_string ""

	push eax
	call release_operand
	add esp, 4

	endFunc

end_pop_and_print:
	print_string "Error: Not Enough Arguments on Stack"
	endFunc






;-----------------------------------------------------------------------------------

release_operand:
	
	startFunc
	mov ecx, dword [ebp+8]		;get first argument to release
	mov ebx, dword [ebp+8]

	cmp ecx, 0
	je end_release_number

	mov dword ecx, [ecx+1]		;enhance the pointer to next link

	pushad
	push ecx
	call release_operand		;recursive calling to this function
	add esp, 4
	popad

	push ebx
	call free
	add esp, 4

	end_release_number:
	endFunc










;-----------------------------------------------------------------------------------

pop_operand:

  startFunc
  xor ebx, ebx

  mov bl, [stack_counter]				;check if stack is empty
  cmp bl, 0
  je stack_is_empty

  dec bl
  mov eax, dword [operands_stack + 4*ebx]					;insert the operand into the top of stack

  mov byte [stack_counter], bl

  mov [tmp_pointer], eax
  popad									;Restore registers
  mov eax, [tmp_pointer]
  mov	esp, ebp						;Function exit code
  pop	ebp	
  ret


stack_is_empty:
  popad									;Restore registers
  xor eax, eax
  mov	esp, ebp						;Function exit code
  pop	ebp	
  ret







;-----------------------------------------------------------------------------------

func_quit:
	
	startFunc

more_operands_in_stack:	
	
	mov cl, [stack_counter]
	cmp cl, 0
	je ret_to_quit

	call pop_operand
	push eax
	call release_operand
	add esp, 4

	jmp more_operands_in_stack

ret_to_quit:
	endFunc





;-----------------------------------------------------------------------------------
func_insert_stack:
  
  startFunc
  mov ecx, dword [ebp+8]
  xor ebx, ebx

  mov bl, [stack_counter]				;check if stack is full
  cmp bl, 4
  ja stack_is_full

  mov dword [operands_stack + ebx*4], ecx					;insert the operand into the top of stack

  inc byte [stack_counter]

  endFunc


stack_is_full:

  print_string "Error: Stack Overflow"

  push ecx
  call release_operand
  add esp, 4
  
  endFunc






;----------------------------------------------------------------------------------------

func_print_number:
	

	startFunc
	mov ecx, dword [ebp+8]		;get first argument to print
	xor ebx, ebx

	
	mov byte bl, [ecx]

	cmp dword [ecx+1], 0
	je last_element_print
	
	mov dword ecx, [ecx+1]		;enhance the pointer to next link

	pushad
	push ecx
	call func_print_number		;recursive calling to this function
	add esp, 4
	popad

print_regular:
	push ebx
	push P_INT
	call printf
	add esp, 8
	endFunc

last_element_print:
	cmp byte [ecx],16
	jb print_without_zero
	jmp print_regular

print_without_zero:
	push ebx
	push P_INT_0
	call printf
	add esp, 8
	endFunc





;--------------------------------------------------------------------------------------


handle_number_from_user:
	
	startFunc
    mov ecx, dword [ebp+8]		;get user string number
	mov dword [tmp_pointer], 0	;initial tmp pointer to zero
	xor esi, esi				;reset counter of data in link
	
	push ecx
	call check_length_is_odd
	add esp, 4

	cmp eax, 0
	je while_not_end_of_line	;if the length number is even jump to while

	pushad
	push dword LINK_SIZE
	call malloc					;get memory for the first link
	mov dword [eax+1], 0 
	mov	[tmp_pointer], eax   	; insert new link into linked list
	add esp, 4
	popad
	xor ebx, ebx
	inc esi
	

while_not_end_of_line:

	cmp byte [ecx], 0			;if it is the end of line
	je end_of_line

	cmp si, 0					;if there is already exist link which is not full
	ja no_new_link

	insert_new_link
	
	xor ebx, ebx

no_new_link:
	
	cmp byte [ecx], 58			;if it is number between 0 to 9
	jb dec_number

	shl bl, 4					;if it number between A to F
	mov bh, byte [ecx]
	sub bh, 55
	xor bl, bh
	xor bh, bh

	cmp esi, 1
	je reset_counter			;if the number of numbers in this link is 2 then go to new link

	inc esi
	inc ecx

	jmp while_not_end_of_line


dec_number:
	
	shl bl, 4
	mov bh, byte [ecx]
	sub bh, 48
	xor bl, bh
	xor bh, bh
	
	cmp esi, 1
	je reset_counter

	add esi, 1
	inc ecx

	jmp while_not_end_of_line


end_of_line:

	mov eax, [tmp_pointer]
	mov [eax], bl 				;insert the hex num into the data field in link
	jmp ret_label


reset_counter:
	
	xor esi, esi
	mov eax, [tmp_pointer]
	mov [eax], bl 				;insert the hex num into the data field in link
	inc ecx
	jmp while_not_end_of_line


ret_label:
	popad						;Restore registers
	mov eax, [tmp_pointer]
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret




;------------------------------------------------------------------------------------

check_length_is_odd:

	startFunc
	xor eax,eax
	mov ecx, dword [ebp+8]

real_number:
	cmp byte [ecx], 0
	jne add_one
	jmp check_odd

add_one:
	inc eax
	inc ecx
	jmp real_number

check_odd:
	mov ebx, 2
	div bl
	cmp ah, 0
	je ret_even

	popad
	mov eax, 1
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret

ret_even:
	popad
	mov eax, 0
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret	





;--------------------------------------------------------------------------------------

check_shorter:

	startFunc
	mov ebx, [ebp+8]
	mov ecx, [ebp+12]

start_again:
	cmp ebx, 0
	je ebx_is_shorter
	cmp ecx, 0
	je ecx_is_shorter
	mov ebx, [ebx+1]
	mov ecx, [ecx+1]
	jmp start_again

ebx_is_shorter:
	popad
	mov eax, 1					;return 1 if the second push argument is shorter
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret	


ecx_is_shorter:
	popad
	mov eax, 0					;return 0 if the first push argument is shorter
	mov	esp, ebp				;Function exit code
	pop	ebp
	ret



;--------------------------------------------------------------------------------------
num_of_links:
	startFunc
	mov ebx, [ebp+8]
	mov byte [links_counter], 0

start_again_count_links:
	cmp ebx, 0
	je end_count
	inc byte [links_counter]
	mov ebx, [ebx+1]
	jmp start_again_count_links

end_count:
	endFunc

