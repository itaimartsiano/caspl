%macro startFunc 0
	push ebp
	mov ebp,esp
%endmacro

%macro endFunc 0
	mov esp,ebp
	pop ebp
	ret
%endmacro


%macro printstring 2
     %%tmp:
          DB %1 ,10, 0
     pushad
     push %2
     push %%tmp
     call printf
     add esp, 8
     popad
%endmacro


%macro printError 1
     pushad
     push %1
     call printf
     add esp, 4
     popad
%endmacro



section	.rodata

STKSZ equ 5

NEXT equ 1

DATA equ 0

newLine:
          DB    10,0

LC3:
    DB "the result is: %X",10,0

PRINT_DATA:
     DB   "%02X", 0, 0   ; Format string

NOT_ENOUGH_ARGS:
     DB   "Error: Insufficient Number of Arguments on Stack", 10, 0				; Format string

STACK_OVERFLOW:
     DB   "Error: Stack Overflow", 10, 0										; Format string

 Calc:
          DB     "calc: ", 0													; Format string

INVALID_INPUT:     
          DB     "Error: Invalid input ",10,0									; Format string

isNumber: DB "Number is ",10,0
isQ: DB "is Quit",10,0
isPlus: DB "is +",10,0
isPrint: DB "is print",10,0
isD: DB "is duplicate",10,0
isT: DB "is power",10,0
isV: DB "is V",10,0
isN: DB "is N",10,0
isSq: DB "is sqr",10,0

section .data

elements_count DD 0 	;number of elements in the stack (CALC_STACK)
operation_count DD 0 	;number of operations of the calculator
links_count DD 0      ;number of elements in the stack (CALC_STACK)
temp  DD 0
extra_zero DD 1          ;boolean number that tell us if we need to add zero to the number that wes given by the user



section .bss
user_input resb 80  ;the input from the user


CALC_STACK: 
	RESD    STKSZ  ;the stack we will use in the calculator (size 5)

length resb 1  

 head  resb 4 	


section .text
	align 16
     global main
     extern printf
     extern malloc
     extern gets
main:
     call my_calc

     ;handle return from my_calc print and stuff

     mov eax,1
     mov ebx,0
     int 0x80

    
 
input:
	startFunc
	push user_input          ;the string that the user will write
    call gets
    add esp,4
	endFunc

	
my_calc:
     startFunc
     
    mov byte [extra_zero],0
    mov byte [elements_count],0
    mov byte [links_count],0

menu_loop:
    
    push    Calc
    call    printf
    add esp, 4

    pushad
     call input
     popad

	 cmp byte[user_input],0  ; if the input is null we ignore it
      je menu_loop

     cmp byte[user_input],'+'  ; if the command is +     
     jne continue1
     call add                    ; call to the command that adds the numbers
     jmp menu_loop
continue1:
     cmp byte[user_input],'p'  ; if the command is pop and print
     jne continue2
     call print                  ; jump to the command that pop and print a number
     jmp menu_loop
continue2:
     cmp byte[user_input],'d'  ; if the command is duplicate  
     jne continue3
     call duplicate              ; jump to the command that duplicate
     jmp menu_loop
continue3:
     cmp byte[user_input],'^'  ; if the command is power
     jne continue4
     call power                  ; jump to the command that performs power
     jmp menu_loop
continue4:
     cmp byte[user_input],'v'  ; if the command is power when Y=(-Y)
     jne continue5
     call minus_power            ; jump to the command that performs power with Y as above
     jmp menu_loop
continue5:
     cmp byte[user_input],'n'  ; if the command is number of '1' bits
     jne continue6
     call num_of_1               ; jump to the command that calculate number of 1 bits
     jmp menu_loop
continue6:
     cmp dword[user_input],'sr' ; if the command is square root
     jne continue7
     call square_root            ; jump to the command square root
     jmp menu_loop
continue7:
     cmp byte[user_input],'q'  ; if the command is quit 
     jne continue8
     call end
continue8:     
     jmp number                ; it is a number and not a command      




     endFunc



add: 
    startFunc
     
     push isPlus
     call printf
     add esp,4

    endFunc
;------------------------------------------------------------------------------------------
;--------------------------------------POP & PRINT function------------------------------
 print: 
    startFunc

	cmp byte[elements_count],0				     ; check if there is an element in the stack to pop and print it
	jle not_enough_args
    
    mov eax, 0
    mov ebx, 0                                   ; Setting to null all the registers for the print functions  
    add     dword[operation_count],1             ; update operation counter
    mov eax, [elements_count]                    ; put the element counter in eax for better handeling with it
    dec eax                                      ; decreasing the number of elements in the stack  
    mov ebx, dword [CALC_STACK + 4*eax]          ; poping the last element to print                                  
    mov [elements_count] , eax                   ; saving the new number of elements in the counter
    mov ecx, 0                                   ; intialize ecx (we will put the data for print in it)
    mov cl,[ebx+DATA]                            ; move the data of the first link into cl
    mov ebx, dword [ebx+NEXT]                    ; advance ebx to the next link
    cmp ebx,0                                    ; check if this link points to null (if this is the only link in the list)
    je printEnd                                  ; if this is the only link we will print her
    pushad                                       ; backup registers
    push ebx                                     ; push the address of the next link
    call printRec
    add esp,4
    popad                                        ; restore registers


printEnd:
    push ecx                                     ; push ecx which contains tha data for printing
    push PRINT_DATA                              ; push the format of printing
    call printf
    add esp,8
    push newLine                                 ; this is the end of the number so we print a newline marker
    call printf
    add esp,4
     endFunc
;-------------------- recursive printing function fo the list------------
printRec:
    startFunc
    mov ebx,[ebp+8]                      ; puts the address of the link in ebx
    mov ecx, 0                           ; intialize ecx (we will put the data for print in it)
    mov cl,[ebx+DATA]                    ; move the data of the link into cl
    mov ebx, dword [ebx+NEXT]            ; advance ebx to the next link
    cmp ebx,0                            ; check if this link points to null 
    je printNow                          ; if the next link is null we have reached the end of the list and now we will print it
    pushad                               ; if it's not the end we backup registers
    push ebx                             ; push the address of the next link 
    call printRec
    add esp,4
    popad                                ; restore registers
printNow:
    push ecx                             ; push ecx which contains tha data for printing
    push PRINT_DATA                      ; push the format of printing
    call printf
    add esp,8
    endFunc


;----------------------------------------------------------------------------------------
duplicate: 
    startFunc

     push isD
     call printf
     add esp,4

    endFunc

power: 
    startFunc

     push isT
     call printf
     add esp,4

    endFunc 

minus_power: 
    startFunc

     push isV
     call printf
     add esp,4

    endFunc 

num_of_1: 
    
    startFunc

     push isN
     call printf
     add esp,4

    endFunc

square_root: 
    startFunc

     push isSq
     call printf
     add esp,4

    endFunc
;--------------------------handle numbers--------------------------------------------------
number: 
    cmp byte [elements_count],5        ; check if there is still room in the stack
    je no_more_room                    ; can't insert the stack,it's already full

    pushad
    mov byte[extra_zero],0              ; initialize the boolean variable
    push user_input
    call check_length                   ; calculate the length of the string
    add esp,4
    mov dword [length], 0               ; initialize the variable that holds the length
    mov dword [length],eax              ; put the result of check_length function to length
    popad
    mov ebx,user_input 
    mov eax,dword [length]              ;lines 208-211 check if the length is even or odd
    mov cl,2
    div cl
    cmp ah,0
    jne odd                            ; if equal jump to even
    jmp parse_number


odd:
     mov byte [extra_zero],1
     jmp parse_number

;-----------------calculating the length of the string----------------------------
check_length:
     startFunc
     mov ebx,[ebp+8]           ; puts the input in ebx
     mov eax,0                           ; initialize length counter
still_loop:
     cmp byte[ebx], 0                   ; if the pointer is in the end of the number
     je end_length
     inc ebx                            ; increase the pointer
     inc eax                            ; increase the counter
     jmp still_loop                       ; continue the loop

end_length:
     endFunc
;------------------------------------------------------------------------------

;------------------parsing the number and inserting it into the stack----------- 
parse_number:
     mov eax,0								; initialize eax (we will save there the data that is in the link)
     cmp byte[extra_zero],0					; check if the number is odd
     jne parse_odd_number					; if it is odd jump to parse_odd_number
     mov edx,1								; initialize the counter of char in a couple
     jmp parse_loop							

parse_odd_number:							; if it is an odd number we add a zero (eax already 0) and parse the second char
    mov edx,2								; initialize the counter of char in a couple so it would start from the second char
     
parse_loop:
     mov ecx,0								; initialize ecx (we will use it later int shl)
     cmp byte [ebx],0						; check if the input is finished (if the pointer points to null)
     je enter_stack							; if we finished parsing the input we will jump to enter_stack

     cmp byte[ebx],'9'        ; check if the char is a digit
     jle from_0_to_9
     cmp byte[ebx],'F'        ; check if the char is a capitel letter
     jle from_A_to_F
     jmp illigal_input        ; if either than it is illigal
enter_list:
     cmp edx,1
     je first                 ; it is the first char in the couple
     jmp second               ; it is the second char in the couple

	
from_0_to_9:                  
     cmp byte[ebx],'0'        ; if it is smaller then 0 it is illigal
     jl illigal_input
     sub byte[ebx],'0'        ; if it is a digit we subtract the ascii value of 0 from it to have the actual number
     jmp enter_list           ; jump to enter the number

from_A_to_F:
     cmp byte[ebx],'A'        ; if it is smaller then A it is illigal
     jl illigal_input
     sub byte[ebx],55        ; if it is between A and F we subtract the ascii value of A from it to have the actual number
     jmp enter_list           ; jump to enter the number
     
first:                        ; the first number to add to the data of the link
     mov al,byte[ebx]         ; put in al the number we computed
     mov cl,4                 ; initialize cl for the shift
     shl	al,cl               ; shift left al 4 times (to make room for the second number later)
     inc edx                  ; increase the counter of char in a couple
     inc ebx                  ; increase the pointer of the input
     jmp parse_loop           ; return to parse the second char

second:                       ; the second number to add to the data of the link
     mov ah,byte[ebx]         ; put the second number in ah
     add al,ah                ; add the second number to the first
     mov edx,1                ; initialize edx (counter of a char in a couple) to 1 
     inc ebx                  ; increase the pointer of the input
     pushad                   ; backup registers
     push eax                 ; push argument 1 for create_link function
     call create_link         ; call the function (to create the link)
     add esp,4                ; delete the function argumant
     popad                    ; restore registers
     mov eax,0                ; initialize eax for next couple of numbers
     jmp parse_loop           ; return to the loop that parse the input

	
;------- create a link in the list of the number----------------
create_link:
     startFunc
     mov ecx,[ebp+8]          ; get the first argument (the data of the link)
     cmp cl,0                 ; checks if it is a couple of 00
     je check_zeros			  ; if it is 2 zeros we will chack if they are the first we ignore else we enter them to the list
continue_link:
     push ecx                 ; backup the register ecx that holds tha data
     push STKSZ               ; push the size of the link we want 5 
     call malloc              
     add esp,4
     pop ecx                  ; restore ecx
check1:
     mov [eax],cl             ; put in the first byte of the link (pointed by eax) the data
     cmp byte [links_count],0 ; check if this is the first link in the list
     je first_link
     jmp next_link

first_link:                             ; make the first link
     mov ebx, dword[links_count]        ; these three lines increase the conter of links in the list by one
     inc ebx
     mov [links_count],dword ebx
     mov dword[eax+1],0                 ; make the pointer for next link null because this link is the first
     mov [head],eax                     ; make head point to this link
     jmp end_link

next_link:                              ; make link
     mov ebx,dword[head]                ; make ebx hold the address of the head os the list
     mov [eax+1],  ebx                  ; new link now will point to the head of the list
     mov dword[head],eax                ; make the head point to this link now
     jmp end_link
end_link:
	 endFunc

check_zeros:
	cmp byte [links_count],0 ; check if this is the first link in the list
	je end_link
	jmp continue_link

;----------------enter the list of numbers to the array (stack)------------------------
enter_stack:   
	 mov esi,0                               
     mov dword esi, [elements_count]         
     mov ebx,dword [head]
     mov [CALC_STACK + esi*4],dword ebx		; inserting the list of the numbers pointed by head (now in ebx) to the right place (according to the number of elements in the stack)
     inc esi								; increase the number of elements in the stack by 1
     mov dword [elements_count],esi
     mov dword [head],0						; reset the pointer head for the next number
     mov dword [links_count],0				; reset the counter of links in a list for the next number
     jmp menu_loop							; return to the main loop
;------------------------------------------------------------------------------------------------------------






illigal_input:

	printError INVALID_INPUT
    jmp menu_loop

not_enough_args:

	printError NOT_ENOUGH_ARGS
    jmp menu_loop

no_more_room:

	printError STACK_OVERFLOW
    jmp menu_loop

end:
	 endFunc